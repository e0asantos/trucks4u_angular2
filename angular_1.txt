html principal
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBwdKhkBS0VwUhiX8beHePaHmjV6WIuVTs&sensor=true"></script>
<script src="https://js.pusher.com/3.1/pusher.min.js"></script>

vista
<div id="map" style="width: 100%;height: 100%;"></div>  




  $scope.onPageLoaded=function() {
    console.log("I'm alive user!");
    this.loadMap();
  }
 
 ----> constructor(nav,navParams,http,requestOptions,headers) {
    
    console.log("current URL:"+SERVER_URL+"usuario:");
    console.log(window.user_session);
    Pusher.logToConsole = true;
    var pusher = new Pusher('cb83ffb2e230419bfaeb', {
      encrypted: true
    });
    var channel2 = pusher.subscribe('all_'+this.currentUser.content.id);
    var channel = pusher.subscribe('client_'+this.currentUser.content.id);
    channel.bind('draw_truck', this.addTrackingMarker);
    channel.bind('no_driver', this.gotNoDriverPush);
    channel.bind('accepted_order', this.gotOrderAccepted);
  }
  $scope.addTrackingMarker=function(data){
    var myLatLng = {lat: Number(data.lat), lng: Number(data.lon)};
    
    var marker = new google.maps.Marker({
      position: myLatLng,
      map: window.this.map,
      title: 'Rastreo',
      icon:"img/small-circle.png",
      animation: google.maps.Animation.DROP,
    });
    
  }
  $scope.gotOrderAccepted=function(data){
    //todo
  }
  $scope.gotNoDriverPush=function(data){
    window.avoidLoading=true;
    if (window.loading!=undefined) {
      window.loading.dismiss();  
    }
    //alerta de no vehiculo
  }
  $scope.addMarker=function(){
	  var marker = new google.maps.Marker({
	    map: this.map,
	    animation: google.maps.Animation.DROP,
	    position: this.map.getCenter()
	  });

	  var content = "<h4>Information!</h4>";          
	  
  }
  
  

  $scope.gotResponseFromOrderRequest=function(data){
    console.log(data);
    if(window.avoidLoading==false){
      window.loading= Loading.create({
        content: "Buscando conductor..."
      });
      this.nav.present(window.loading);
    }
    window.avoidLoading=false;
  }
  $scope.loadMap=function(){
 	
        var minZoomLevel = 12;
        var currentPos=new google.maps.LatLng(25.67593049605625,-100.41711058462803);
        if(window.GPSPosition!=undefined){
          currentPos = new google.maps.LatLng(window.GPSPosition.coords.latitude,window.GPSPosition.coords.longitude);
        }
        this.map = new google.maps.Map(document.getElementById('map'), {
            zoom: minZoomLevel,
            center: currentPos,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            styles:window.empakingStyle
        });
        console.log("map-loaded");
  }


