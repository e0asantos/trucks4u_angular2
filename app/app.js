import {ViewChild} from '@angular/core';
import {App, Platform} from 'ionic-angular';
import {StatusBar} from 'ionic-native';
import {GettingStartedPage} from './pages/getting-started/getting-started';
import {ListPage} from './pages/list/list';
import {LoginScreen} from './pages/loginScreen/loginScreen';
import {WelcomeScreen} from './pages/welcomeScreen/welcomeScreen';
import {HistoryOrder} from './pages/historyOrder/historyOrder';
import {RequesterScreen} from './pages/requesterScreen/requesterScreen';
import {DispatchPage} from './pages/dispatch/dispatch';
import {ConversationPage} from './pages/conversation/conversation';
import {DriverScreen} from './pages/driverScreen/driverScreen';
import {UserDetailsPage} from './pages/user-details/user-details';
import {LogoutPage} from './pages/logout/logout';


@App({
  templateUrl: 'build/app.html',
  config: {}, // http://ionicframework.com/docs/v2/api/config/Config/
  queries: {
    nav: new ViewChild('content')
  }
})
class MyApp {
  
  static get parameters() {
    return [[Platform]];
  }

  constructor(platform) {
    this.platform = platform;
    window.thisAppjs=this;
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages=[];
    var storage = window.localStorage;
    var mroute = storage.getItem("welcome1");
    if (mroute==null || mroute==undefined) {
      this.rootPage = WelcomeScreen;  
    } else {
      this.rootPage = LoginScreen;
    }

    
    
  }

  menuConfigurator(settings){
    if (settings=="driver") {
      this.pages = [
      { title: 'En proceso', component: DriverScreen },
      { title: 'Historial', component: HistoryOrder },
      { title: 'Chat', component: ConversationPage },
      { title: 'Perfil', component: UserDetailsPage },
      { title: 'Salir', component: LoginScreen }
    ];
  } else {
    this.pages = [{ title: 'Pedir recolector', component: RequesterScreen },
      { title: 'Historial', component: HistoryOrder },
      { title: 'Chat', component: ConversationPage },
      { title: 'Perfil', component: UserDetailsPage },
      { title: 'Salir', component: LoginScreen }
    ];
  }
    
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      // StatusBar.styleDefault();
      StatusBar.overlaysWebView(false);
      window.masterPosition();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
