import {Page, NavController} from 'ionic-angular';
import {Http, RequestOptions,Headers} from '@angular/http';
import {SERVER_URL} from '../../services/server-config';
import {NEO} from '../../services/server-config';
/*
  Generated class for the ConversationPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Page({
  templateUrl: 'build/pages/conversation/conversation.html',
})
export class ConversationPage {
  static get parameters() {
    return [[NavController],[Http]];
  }

  isMessageCreator(uid){
    if (this.currentUser.content.id==Number(uid)) {
      return "sent";
    }
    return "received";
  }

  constructor(nav,http) {

    window.cthis=this;
    this.nav = nav;
    this.wholeConversation=[];
    this.chat=new Object();
    this.http=http;
    this.currentUser=window.user_session;
    var pusher = new Pusher('cb83ffb2e230419bfaeb', {
      encrypted: true
    });
    var channel = pusher.subscribe('client_'+this.currentUser.content.id);
    channel.bind('chat', (data)=>this.gotMessageFromChat(data));
    var canalGlobal = pusher.subscribe('dispatchApp');
    canalGlobal.bind('chat', (data)=>this.gotMessageFromChat(data));
  }

  gotMessageFromChat(data){
    console.log("chat-->"+data)
    this.wholeConversation.push(data);
  }

  sendMessage(){
    console.log(this.chat);
    this.chat.authenticity_token=window.TOKEN;
    var body = JSON.stringify(this.chat);
    var headers = new Headers({ 'Content-Type': 'application/json' });
    var options = new RequestOptions({ headers: headers });
    if (NEO==true) {
      this.http.post(SERVER_URL+'/TruckFinder/Message?driverID='+this.currentUser.content.id+'&message='+this.currentUser.content.name+":"+this.chat.message,body , options)
    .subscribe(
      data=>this.gotResponseFromChat(data),
      err => this.logError(err.json().message),
      () => console.log('Authentication Complete')
    );
    } else {
      this.http.post(SERVER_URL+'/usuarios/chat.json',body , options)
    .subscribe(
      data=>this.gotResponseFromChat(data),
      err => this.logError(err.json().message),
      () => console.log('Authentication Complete')
    );
    }
    
    this.chat.message="";
  }
  logError(data){
    console.log(data);
  }
  gotResponseFromChat(data){

  }
}
