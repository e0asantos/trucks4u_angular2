import {Page, NavController} from 'ionic-angular';

/*
  Generated class for the DispatchPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Page({
  templateUrl: 'build/pages/dispatch/dispatch.html',
})
export class DispatchPage {
  static get parameters() {
    return [[NavController]];
  }

  constructor(nav) {
    this.nav = nav;
    this.currentUser=window.user_session;
    this.something=new Object();
    this.mdata = [
    {
      name: 'Day 1',
      points: [
        {
          filled: true,
          img: 'images/avatar-01.png',
          time: this.genrateDate('09:00:00'),
          speaker: {
            name: 'Adrian Smith',
            img: 'images/avatar-01.png',
            job: 'IT & Network',
          },
          desc: 'Enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
        },
        {
          filled: true,
          time: this.genrateDate('10:00:00'),
          speaker: {
            name: 'Adrian Smith',
            img: 'images/avatar-01.png',
            job: 'IT & Network',
          },
          desc: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.'
        },
        {
          filled: true,
          time: this.genrateDate('10:30:00'),
          speaker: {
            name: 'Adrian Smith',
            img: 'images/avatar-01.png',
            job: 'IT & Network',
          },
          desc: 'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
        },
        {
          filled: false,
          time: this.genrateDate('11:30:00'),
          speaker: {
            name: 'Adrian Smith',
            img: 'images/avatar-01.png',
            job: 'IT & Network',
          },
          desc: 'Sunt in culpa qui officia deserunt mollit anim id est laborum.'
        },
        {
          filled: true,
          time: this.genrateDate('12:15:00'),
          title: 'Lunch',
          desc: 'Lunch time!'
        },
        {
          filled: false,
          time: this.genrateDate('13:15:00'),
        },
        {
          filled: true,
          img: 'images/avatar-02.png',
          time: this.genrateDate('14:00:00'),
          speaker: {
            name: 'Sarah Spencer',
            img: 'images/avatar-02.png',
            job: 'Designer'
          },
          desc: 'Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
        },
        {
          filled: true,
          time: this.genrateDate('15:00:00'),
          speaker: {
            name: 'Sarah Spencer',
            img: 'images/avatar-02.png',
            job: 'Designer'
          },
          desc: 'Excepteur sint occaecat cupidatat non proident.'
        },
      ]
    },
    {
      name: 'Day 2',
      points: [
        {
          filled: true,
          img: 'images/avatar-03.png',
          time: this.genrateDate('09:00:00'),
          speaker: {
            name: 'Emma Henry',
            img: 'images/avatar-03.png',
            job: 'Developer'
          },
          desc: 'Cillum dolore eu fugiat nulla pariatur, excepteur sint occaecat cupidatat.'
        },
        {
          filled: true,
          time: this.genrateDate('10:30:00'),
          speaker: {
            name: 'Emma Henry',
            img: 'images/avatar-03.png',
            job: 'Developer'
          },
          desc: 'Duis aute irure dolor in reprehenderit in voluptate velit esse.'
        },
        {
          filled: false,
          time: this.genrateDate('11:30:00'),
          speaker: {
            name: 'Emma Henry',
            img: 'images/avatar-03.png',
            job: 'Developer'
          },
          desc: 'Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
        },
        {
          filled: true,
          time: this.genrateDate('12:00:00'),
          title: 'Lunch',
          desc: 'Lunch time!'
        },
        {
          filled: false,
          time: this.genrateDate('13:00:00')
        },
        {
          filled: true,
          img: 'images/avatar-04.png',
          time: this.genrateDate('14:00:00'),
          speaker: {
            name: 'Arthur Reed',
            img: 'images/avatar-04.png',
            job: 'Developer'
          },
          desc: 'Incididunt ut labore et dolore ut enim ad minim veniam.'
        },
        {
          filled: true,
          time: this.genrateDate('14:30:00'),
          speaker: {
            name: 'Arthur Reed',
            img: 'images/avatar-04.png',
            job: 'Developer'
          },
          desc: 'Aliquip aute irure dolor in reprehenderit in voluptate velit.'
        },
        {
          filled: true,
          time: this.genrateDate('15:00:00'),
          speaker: {
            name: 'Arthur Reed',
            img: 'images/avatar-04.png',
            job: 'Developer'
          },
          desc: 'Voluptate velit esse cillum dolore eu fugiat.'
        },
      ]
    },
    {
      name: 'Day 3',
      points: [
        {
          filled: true,
          img: 'images/avatar-05.png',
          time: this.genrateDate('09:00:00'),
          speaker: {
            name: 'Christian Mitchell',
            img: 'images/avatar-05.png',
            job: 'DB Manager'
          },
          desc: 'Cupidatat non proident, sunt in culpa qui officia deserunt mollit.'
        },
        {
          filled: true,
          time: this.genrateDate('10:00:00'),
          speaker: {
            name: 'Christian Mitchell',
            img: 'images/avatar-05.png',
            job: 'DB Manager'
          },
          desc: 'Cillum dolore eu fugiat nulla pariatur.'
        },
        {
          filled: true,
          time: this.genrateDate('11:00:00'),
          speaker: {
            name: 'Christian Mitchell',
            img: 'images/avatar-05.png',
            job: 'DB Manager'
          },
          desc: 'Lorem ipsum dolor sit amet sed do eiusmod tempor incididunt.'
        },
      ]
    }
  ];

  this.speakerTmpl =
    '<a href="javascript:void(0)" class="speaker">' +
      '<img src="{img}" alt="">' +
      '<div class="speaker-inner">' +
        '<h3 class="speaker-name">{name}</h3>' +
        '<span class="speaker-job">{job}</span>' +
      '</div>' +
    '</a>';

    
    
  }
  ngAfterViewInit2() {
    console.log("ngAfterViewInit")
    this.timelinzElem = D.id('timeline');
    this.timelinz = new Timelinz(this.mdata, {
      intervalUnit: 'hour',
      extraIntervals: [0, 0],
      marginBetweenLines: 70,
      intervalFormat: this.dateFormat,

      onLineRendered:  (lineElem, lineData)=> {
        // Add timeline title
        var lineTitleElem = D.compile('<h4 class="timelinz__line-title">{name}</h4>', {name: lineData.name});
        D.prepend(lineElem, lineTitleElem);
      },

      onPointRendered:  (pointElem, pointData, timelineData)=> {
        // Set tooltip
        var tooltip = this.dateFormat(new Date(pointData.time));
        if (pointData.title) {
          tooltip += ' - ' + pointData.title;
        } else {
          tooltip += pointData.speaker ? ' - ' + pointData.speaker.name : '';
        }
        D.classList.add(pointElem, 'tooltip-left');
        D.attrs(pointElem, {'data-tooltip': tooltip});

        // Append <span> to normal points
        if (!pointData.img) {
          D.append(pointElem, document.createElement('span'));
        }
      },

      onPointClicked: (e, pointData, timelineData)=> {
        e.preventDefault();
        this.selectPoint(pointData, timelineData);
      },

      onRenderCompleted:  ()=> {
        // Choose default point
        this.selectPoint(this.mdata[0].points[0], this.mdata[0]);
      }
    });
    this.timelinz.render(this.timelinzElem);
  }
  genrateDate (time) {
    var toInt = function (i) {
      return +i;
    };
    var date = (new Date()).toJSON().substr(0, 10).split('-').map(toInt);
    time = time.split(':').map(toInt);
    return new Date(date[0], date[1] - 1, date[2], time[0], time[1], time[2]);
  }

  pad (num, count) {
      return ((new Array(count + 1)).join('0') + num).slice(count * -1);
  };

  dateFormat (date) {
    var fpart=((new Array(3)).join('0') + date.getHours()).slice(2 * -1);
    var spart= ((new Array(2 + 1)).join('0') + date.getMinutes()).slice(2 * -1);
    return fpart + ':' + spart;
    //return this.pad(date.getHours(), 2) + ':' + this.pad(date.getMinutes(), 2);
  };

  selectPoint  (pointData, timelineData) {
    // Set point title
    var pointTitleElem = D.id('point-title');
    var title = '';
    if (pointData.title) {
      title += pointData.title + ' at ';
    }
    title += this.dateFormat(new Date(pointData.time)) + ' - ' + timelineData.name;
    pointTitleElem.textContent = title;

    // Set point description
    D.id('point-desc').textContent = pointData.desc;

    // Set speakers
    var speakersTitleElem = D.id('speakers-title');
    var speakersElem = D.id('speakers');
    D.empty(speakersElem);
    if (pointData.speaker) {
      D.classList.remove(speakersTitleElem, 'hidden');
      D.append(speakersElem, D.compile(this.speakerTmpl, pointData.speaker));
    } else {
      D.classList.add(speakersTitleElem, 'hidden');
    }
  };

  


}
