import {Page,Alert,NavController,NavParams,Modal,ViewController,Toast} from 'ionic-angular';
import { Inject } from '@angular/core';
import {Http, RequestOptions,Headers} from '@angular/http';
import {SERVER_URL} from '../../services/server-config';
import {PromptForOrder} from './promptForOrder';
import {PromptForSignature} from './promptForSignature';
import {NEO} from '../../services/server-config';
import {ConversationPage} from '../conversation/conversation';
@Page({
  inputs: ['isDisabled'],
  templateUrl: 'build/pages/driverScreen/driverScreen.html'
})
export class DriverScreen {
  onPageLoaded() {
    console.log("I'm alive2!");
    this.loadMap();
    //this.openUpCheckList();
    if (window.masterCheckList==undefined) {
      setTimeout(() => this.openUpCheckList(), 2000);
      window.masterCheckList=1;  
    }
    if (window.currentOrderInDispatch==undefined && this.map!=null && this.map!=undefined) {
      this.getOrderInProgress();
    }
  }
  static get parameters() {
    return [[NavController], [NavParams],[Http],[RequestOptions],[ViewController]];
  }
  gotoChat(){
    this.nav.push(ConversationPage, {});
  }
  openUpCheckList(){
    let alert = Alert.create();
    alert.setTitle('Revise su vehiculo y seleccione si el estado de la opción es adecuado para conducir.');

    alert.addInput({
      type: 'checkbox',
      label: 'Revision de llantas',
      value: 'llantas',
      checked: false
    });

    alert.addInput({
      type: 'checkbox',
      label: 'Revision de gasolina',
      value: 'gasolina',
      checked: false
    });

    alert.addInput({
      type: 'checkbox',
      label: 'Revision de aceite',
      value: 'aceite'
    });

    alert.addInput({
      type: 'checkbox',
      label: 'Revision de frenos',
      value: 'frenos'
    });
    alert.addInput({
      type: 'checkbox',
      label: 'Revision de luces de freno',
      value: 'freno_luz'
    });
    alert.addInput({
      type: 'checkbox',
      label: 'Revision de luces frontales',
      value: 'luz_frontal'
    });

    alert.addButton('Cancel');
    alert.addButton({
      text: 'Okay',
      handler: data => {
        if (data.length==0) {
          this.nav.setRoot(DriverScreen);
          return;
        }
        var resultsa=new Object();
        for (var i = data.length - 1; i >= 0; i--) {
          resultsa[data[i]]=true;
        }
        resultsa["driver"]=1
        resultsa["truck"]=this.currentUser.content.id
        var body = JSON.stringify(resultsa);
        console.log(body)
        var headers = new Headers({ 'Content-Type': 'application/json' });
        var options = new RequestOptions({ headers: headers });
        if (NEO==true) {
          this.http.post(SERVER_URL+'/TruckFinder/InsertCheckList',body , options)
        .subscribe(
          data=> window.this.gotResponseFromCheclist(data),
          err => window.this.logError(err.json().message),
          () => console.log('checklist complete')
        );
        } else {

        }
        // this.http.post(SERVER_URL+'/king_locations.json',body , options)
        // .subscribe(
        //   data=> window.this.gotResponseFromGPS(data),
        //   err => window.this.logError(err.json().message),
        //   () => console.log('gps Complete')
        // );
        console.log('Checkbox data:', data);
        this.testCheckboxOpen = false;
        this.testCheckboxResult = data;
      }
    });
    this.nav.present(alert);
  }
  gotResponseFromCheclist(data){
    console.log(data)
  }
  constructor(nav,navParams,http,requestOptions,controller) {
    window.this=this;
    window.driverScreenjs=this;
  	this.nav=nav;
    this.prevDistance=[];
    this.directionsService = new google.maps.DirectionsService();
    this._directionsRenderer = new google.maps.DirectionsRenderer();
    this.stepNumber=0;
    this.http=http;
    this.telephone="8180829367"
    this.titleOrETA="Servicios"
    this.statusState=0;
    this._mapPoints=[];
    this.fixMacStyle="110px !important";
    window.controller=controller;
    // el objeto de estado de las geofences
    //{geofenceid:1,state:0,lat,long,distance,name}
    this.geofenceState=[];
    
    // if(NEO==true){
    //   window.user_session.content.name="Driver1";
    //   window.user_session.content.lastname="Lastname";
    // }
    this.currentUser=window.user_session;
    this.packagesAvailable=window.packagesAvailable;
    window.closePromptRequest=this.closePromptRequest

    console.log("current URL:"+SERVER_URL);
    Pusher.logToConsole = true;
    var pusher = new Pusher('cb83ffb2e230419bfaeb', {
      encrypted: true
    });
    var channel = null;
    if(NEO==false){
      channel=pusher.subscribe('driver_'+this.currentUser.content.id);
    } else {
      channel=pusher.subscribe('driver_'+this.currentUser.access_token);
    }
    
    channel.bind('draw_truck', function(data) {
      alert(data.message);
    });
    channel.bind('looking_driver', (data)=>this.lookingDriverEventHandler(data));
    channel.bind('forced_order', (data)=>this.gotAcceptedOrder(data));
    channel.bind('order_terminated', (data)=>this.orderTerminated(data));
    window.captureGPS=this.captureGPS;
    
    
    pusher.connection.bind('state_change', function(states) {
      // states = {previous: 'oldState', current: 'newState'}
      window.isUserOnlineWithPush=true;
      if (states.current!="connected") {
        window.isUserOnlineWithPush=false;
      }
      console.log("Pusher's current state is " + states.current);
    }); 
    if(window.masterGPSTimeout==undefined){
      this.captureGPS();
      window.masterGPSTimeout=1;
    }
    
    this.getGeoZones();
    if (this.map!=null && this.map!=undefined) {
      this.getOrderInProgress();
    }
    if (window.thisOS!="macintel") {
    window.notificationOpenedCallback = function(jsonData) {
      console.log('didReceiveRemoteNotificationCallBack: ' + JSON.stringify(jsonData));
    };

    window.plugins.OneSignal.init("e6df78f1-34ff-4041-a31d-c3a62be3fd27",
                                 {googleProjectNumber: "980349758335"},
                                 window.notificationOpenedCallback);
    window.plugins.OneSignal.getIds((ids)=> {
      console.log('getIds: ' + JSON.stringify(ids));
      var infoPush=new Object();
      infoPush.token=ids.pushToken;
      infoPush.device_id=ids.pushTokenDevice;
      infoPush.authenticity_token=window.TOKEN;
      infoPush.utf8="✓"
      var body = JSON.stringify(infoPush);
      var headers = new Headers({ 'Content-Type': 'application/json' });
      var options = new RequestOptions({ headers: headers });
      this.http.post(SERVER_URL+'/push/update.json',body , options)
      .subscribe(
        data=>this.gotResponseFromPush(data),
        err => this.logError(err.json().message),
        () => console.log('Push token sent')
      );
      window.pushToken=ids.pushToken;
      window.pushTokenDevice=ids.userId;
    });
  
   // Show an alert box if a notification comes in when the user is in your app.
   window.plugins.OneSignal.enableInAppAlertNotification(true); 
 }
  }
  gotResponseFromPush(response){

  }
  orderTerminated(data){
    window.currentOrderInDispatch=undefined;
    this.statusState=0;
    this._directionsRenderer.setMap(null);
    this.stepNumber=0;
    this.titleOrETA="Servicios";
    this._directionsRenderer.setMap(null);
  }
  getGeoZones(){
    if (NEO==true) {
      this.http.get(SERVER_URL+'/TruckFinder/Geofences')
      .subscribe(
        data=>this.gotGeoZones(data),
        err => this.logError(err.json().message),
        () => console.log('gps Complete')
    );
    } else {
     this.http.get(SERVER_URL+'/indexall.json')
      .subscribe(
        data=>this.gotGeoZones(data),
        err => this.logError(err.json().message),
        () => console.log('gps Complete')
    ); 
    }
     
  }

   distanceBetweenPoints(lat1, lon1) {
    var lat2=0;
    var lon2=0;
    if (this.prevDistance.length==0) {
      this.prevDistance[0]=lat1;
      this.prevDistance[1]=lon1;
      return 0;
    }
    lat2=this.prevDistance[0];
    lon2=this.prevDistance[1];

    // var R = 6371; // Radius of the earth in km
    // var dLat = this.toRad(lat2-lat1);  // Javascript functions in radians
    // var dLon = this.toRad(lon2-lon1); 
    // var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
    //         Math.cos(this.toRad(lat1)) * Math.cos(this.toRad(lat2)) * 
    //         Math.sin(dLon/2) * Math.sin(dLon/2); 
    // var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    // var d = R * c; // Distance in km
    // this.prevDistance[0]=lat1;
    // this.prevDistance[1]=lon1;
    // return d/1000;//distance in meters


  var p = 0.017453292519943295;    // Math.PI / 180
  var c = Math.cos;
  var a = 0.5 - c((lat2 - lat1) * p)/2 + 
          c(lat1 * p) * c(lat2 * p) * 
          (1 - c((lon2 - lon1) * p))/2;
  this.prevDistance[0]=lat1;
  this.prevDistance[1]=lon1;
  return 12742 * Math.asin(Math.sqrt(a))*1000; // 2 * R; R = 6371 km

  }
  toRad(Value) {
    /** Converts numeric degrees to radians */
    return Value * Math.PI / 180;
  }
  gotGeoZones(data){
    var arr=JSON.parse(data._body);
    if (NEO==true) {
      return;
      arr=arr.Table;
    }
    this.geofenceState=[];
    for (var i = arr.length - 1; i >= 0; i--) {
      arr[i]["state"]=0;
      if (arr[i].fenceradius!=undefined) {
        arr[i].distance=arr[i].fenceradius;
      }
      if (arr[i].fencetype!=undefined) {
        arr[i].geo_type=arr[i].fencetype;
      }
      this.geofenceState.push(arr[i]);
    }
  }
  gotAcceptedOrder(data){
    //window.currentOrderInDispatch=JSON.parse(data._body);
    window.currentOrderInDispatch={id:data.order,points:data.points};
    this.startProcessingRoute(window.currentOrderInDispatch.points);
    //window.captureGPS();
  }
  getOrderInProgress(){
    if(NEO==false){
      this.http.get(SERVER_URL+'/request/inprogress.json')
      .subscribe(
        data=>this.gotOrderInProgress(data),
        err => this.logError(err.json().message),
        () => console.log('gps Complete')
      );  
    } else {
      console.log("accepted--->"+this.currentUser.content.id)
      this.http.get(SERVER_URL+'/TruckFinder/AcceptedOrdersByTruckId/'+this.currentUser.content.id)
      .subscribe(
        data=>this.gotOrderInProgress(data),
        err => this.logError(err.json().message),
        () => console.log('ACCEPTED-LIST')
    );
    }
    
  }
  
  isDriverOnService(){
    if (window.currentOrderInDispatch!=undefined && this.stepNumber==0) {
      return true;
    }
    return false;
  }
  partialShowButton(){
    if (this.stepNumber==0) {
      return true;
    }
    return false;
  }
  showEPOD(){
    if (this.statusState>=10) {
      return true;
    }
    return false;
  }
  gotOrderInProgress(data){
    if (NEO==true) {
      //window.currentOrderInDispatch={id:data.order,points:data.points};
      //var preparse="["+data._body.replace(/'/g,'"')+"]";
      var incoming=JSON.parse(data._body).Table[0];
      if (incoming!=undefined) {
        window.currentOrderInDispatch={order:incoming.idOrder,id:incoming.idOrder,points:incoming.points}
        var storage = window.localStorage;
        var mroute = storage.getItem("route"); // Pass a key name to get its value.
        if (mroute!=undefined || mroute!="") {
          window.currentOrderInDispatch.points=mroute;
          this.startProcessingRoute(mroute);    
        } else {
          window.currentOrderInDispatch.points=incoming.points.replace(/'/g,'"');
          this.startProcessingRoute(incoming.points.replace(/'/g,'"'));    
        }
      }
    } else {
      if (data._body.indexOf("NONE")==-1) {
        window.currentOrderInDispatch=JSON.parse(data._body).order;
        window.currentOrderInDispatch.points=JSON.parse(data._body).points;
        this.startProcessingRoute(JSON.parse(data._body).points);    
      }
      
    }
    this.getETA();
    //this.captureGPS();
  }
  logError(data){
    console.log(data)
  }
  captureGPS(){
    
    // navigator.geolocation.getCurrentPosition(
    //     (position) => {
    //       console.log(position);
    //     },
    //     (error) => {
    //       console.log(error);
    //     }, locationOptions
    //   );
    window.masterPosition();
    this.getETA();
    var storage = window.localStorage;
    var dbGPS = storage.getItem("GPS"); // Pass a key name to get its value.
    if (dbGPS==undefined || dbGPS=="") {
      storage.setItem("GPS", JSON.stringify([]));

    }
    dbGPS=JSON.parse(storage.getItem("GPS"));
    if (window.GPSPosition!=undefined && this.driverMarker!=undefined) {
      var myLatLng = {lat: window.GPSPosition.coords.latitude, lng: window.GPSPosition.coords.longitude};
      this.driverMarker.setPosition(myLatLng);
      this.map.setCenter(myLatLng); 
    }
    if (window.isUserOnlineWithPush==true && dbGPS.length>0) {
      for (var i = dbGPS.length - 1; i >= 0; i--) {
        var body = JSON.stringify(dbGPS[i]);
        var headers = new Headers({ 'Content-Type': 'application/json' });
        var options = new RequestOptions({ headers: headers });
        if(NEO==false){
          window.this.http.post(SERVER_URL+'/king_locations.json',body , options)
        .subscribe(
          data=> window.this.gotResponseFromGPS(data),
          err => window.this.logError(err.json().message),
          () => console.log('gps Complete')
        );
        } else {
          var date=new Date();
          var fecha=date.getFullYear()+"-"+(date.getMonth()+1)+"-"+date.getDate();
          var morden="0";
          if (window.currentOrderInDispatch!=undefined) {
            morden=window.currentOrderInDispatch.order;  
          }
          var mhora1=date.getHours();
          var mhora=mhora1+':'+date.getMinutes();
          if (morden=="" || morden==null || morden==undefined) {
            morden=0;
          }
          var cmdistance=this.distanceBetweenPoints(window.GPSPosition.coords.latitude,window.GPSPosition.coords.longitude);
          //console.log(SERVER_URL+'/TruckFinder/RegisterPosition?date='+fecha+'&time='+mhora+'&idTrucks=2&lat='+window.GPSPosition.coords.latitude+'&lon='+window.GPSPosition.coords.longitude+'&orderID='+morden+'&distance='+cmdistance);
          window.this.http.post(SERVER_URL+'/TruckFinder/RegisterPosition?date='+fecha+'&time='+mhora+'&idTrucks=2&lat='+window.GPSPosition.coords.latitude+'&lon='+window.GPSPosition.coords.longitude+'&orderID='+morden+'&distance='+cmdistance+'&orderStatus='+this.statusState,body , options)
          .subscribe(
            data=> window.this.gotResponseFromGPS(data),
            err => window.this.logError(err.json().message),
            () => console.log('gps Complete')
          );
        }
      }
      storage.removeItem("GPS");
    }
    if (window.GPSPosition!=undefined && window.isUserOnlineWithPush==true) {
      var usercoin=new Object();
      usercoin.lat=window.GPSPosition.coords.latitude;
      usercoin.long=window.GPSPosition.coords.longitude;
      if (window.currentOrderInDispatch!=undefined) {
        usercoin.order=window.currentOrderInDispatch.id;  
      } else {
        usercoin.order=-1
      }
      usercoin.orderStatus=this.statusState;
      var date=new Date();
      var fecha=date.getFullYear()+"-"+(date.getMonth()+1)+"-"+date.getDate();
      var mhora1=date.getHours();
      var mhora=mhora1+':'+date.getMinutes();
      usercoin.time=mhora;
      
      usercoin.authenticity_token=window.TOKEN;
      usercoin.utf8="✓"

      var body = JSON.stringify(usercoin);
      var headers = new Headers({ 'Content-Type': 'application/json' });
      var options = new RequestOptions({ headers: headers });
      if (NEO==false) {
        window.this.http.post(SERVER_URL+'/king_locations.json',body , options)
      .subscribe(
        data=> window.this.gotResponseFromGPS(data),
        err => window.this.logError(err.json().message),
        () => console.log('gps Complete')
      );
      } else {
          
          var morden="0";
          if (window.currentOrderInDispatch!=undefined) {
            morden=window.currentOrderInDispatch.order;  
          }
          
          var cmdistance=this.distanceBetweenPoints(window.GPSPosition.coords.latitude,window.GPSPosition.coords.longitude);
          //console.log(SERVER_URL+'/TruckFinder/RegisterPosition?date='+fecha+'&time='+mhora+'&idTrucks=2&lat='+window.GPSPosition.coords.latitude+'&lon='+window.GPSPosition.coords.longitude+'&orderID='+morden+'&distance='+cmdistance);
          window.this.http.post(SERVER_URL+'/TruckFinder/RegisterPosition?date='+fecha+'&time='+mhora+'&idTrucks=2&lat='+window.GPSPosition.coords.latitude+'&lon='+window.GPSPosition.coords.longitude+'&orderID='+morden+'&distance='+cmdistance+'&orderStatus='+this.statusState,body , options)
          .subscribe(
            data=> window.this.gotResponseFromGPS(data),
            err => window.this.logError(err.json().message),
            () => console.log('gps Complete')
          );
        }
      
      //http://10.15.173.141:81/MySQLAPI/api/TruckFinder/InsertPositions?date=2016-06-01&time=15:50&idTrucks=2&lat=25.70428&lon=-100.41559

      // window.this.http.post("http://10.15.173.141:81/MySQLAPI/api/TruckFinder/InsertPositions?date=2016-06-01&time=15:50&idTrucks="+this.currentUser.content.id+"&lat="+usercoin.lat+"&lon="+usercoin.long,body , options)
      // .subscribe(
      //   data=> window.this.gotResponseFromGPS(data),
      //   err => window.this.logError(err.json().message),
      //   () => console.log('gps Complete')
      // );
      
    } else if(window.GPSPosition!=undefined && window.isUserOnlineWithPush==false){
      var usercoin=new Object();
      usercoin.lat=window.GPSPosition.coords.latitude;
      usercoin.long=window.GPSPosition.coords.longitude;
      usercoin.orderStatus=this.statusState;
      var date=new Date();
      var fecha=date.getFullYear()+"-"+(date.getMonth()+1)+"-"+date.getDate();
      var mhora1=date.getHours();
      var mhora=mhora1+':'+date.getMinutes();
      usercoin.time=mhora;
      if (window.currentOrderInDispatch!=undefined) {
        usercoin.order=window.currentOrderInDispatch.id;  
      } else {
        usercoin.order=-1
      }
      usercoin.authenticity_token=window.TOKEN;
      usercoin.utf8="✓"
      
      dbGPS.push(usercoin)
      storage.setItem("GPS", JSON.stringify(dbGPS)) // Pass a key name and its value to add or update that key.
       // Pass a key name to remove that key from storage.
    }
    if (window.user_session.content.operativ=="online") {
      setTimeout(() => window.this.captureGPS(), 5000);
      this.testPointInCircle();
    } else {
      window.masterGPSTimeout=undefined;
    }
  }
//   1 Pending
// 2 Assigned
// 3 In Route
// 4 Delivered
// 5 Completed
// 6 Cancelled
// 7 Start Loading
// 8 Finish Loading
// 9 Start Unloading
// 10 Finish Unloading
  
  bottomStyleMac(){
    if (window.thisOS=="macintel" || window.thisOS=="iphone") {
      //this.fixMacStyle="110px !important"
      //return "110px;"
      return {buttons_bottom_mac:true,buttons_bottom_phone:false}
    } else {
      //this.fixMacStyle="60px !important"
      return {buttons_bottom_mac:false,buttons_bottom_phone:true}
      //return "60px;"
    }
  }
  startLoading(){
    this.statusState=10;
    this.stepNumber=0;
    if(window.thisOS!="macintel"){
      cordova.plugins.notification.local.schedule({
      title: "Zona de carga",
      message: "Entraste a zona de carga."
      });
    }
    
    setTimeout(() => this.stepNumber=2, 3000);
    
  }
  finishLoading(){
    this.statusState=11;
    this.stepNumber=0
    //setTimeout(() => this.stepNumber=0, 3000);
    
  }
  startUnloading(){
    if (window.thisOS!="macintel") {
      cordova.plugins.notification.local.schedule({
        title: "Zona de descarga",
        message: "Entraste a zona de descarga."
      });  
    }
    
    
    this.statusState=12;
    this.stepNumber=0
    setTimeout(() => this.stepNumber=4, 3000);
    
  }
  finishUnloading(){
    this.statusState=13;
    this.stepNumber=0
    //setTimeout(() => this.stepNumber=0, 3000);
  }
  convertToGeofence(punto,tipo){
    punto["state"]=0;
    punto["distance"]=110;
    punto["geo_type"]=tipo
    this.geofenceState.push(punto);
  }
  startProcessingRoute(points){
    this._directionsRenderer.setMap(this.map);
    this.geofenceState=[];
    var storage = window.localStorage;
    storage.setItem("route", points);

    this.statusState=3;
    var puntos=JSON.parse(points);
    if (puntos==null) {
      return;
    }
    if (puntos.length>2) {
      this.convertToGeofence(puntos[1],1);
      this.convertToGeofence(puntos[2],2);
    } else {
      this.convertToGeofence(puntos[0],1);
      this.convertToGeofence(puntos[1],2);
    }
    
    var correctedParsed=[];
    for (var i=0;i< puntos.length;  i++) {
      puntos[i]
      correctedParsed.push(new google.maps.LatLng(puntos[i].lat, puntos[i].lng))
    }
    this._mapPoints=correctedParsed;
    this.getRoutePointsAndWaypoints(correctedParsed);
  }
  testPointInCircle(){
    // 
    if (window.GPSPosition==undefined || window.GPSPosition==null) {
      return;
    }
    if (window.GPSPosition.coords==undefined || window.GPSPosition.coords==null) {
      console.log("no coords");

    }
    for (var i = this.geofenceState.length - 1; i >= 0; i--) {
      var centro=new google.maps.LatLng(Number(this.geofenceState[i].lat),Number(this.geofenceState[i].lng));
      var currentPos = new google.maps.LatLng(window.GPSPosition.coords.latitude,window.GPSPosition.coords.longitude);
      var tested=(google.maps.geometry.spherical.computeDistanceBetween(currentPos, centro) <= Number(this.geofenceState[i].distance))
      if (tested && window.currentOrderInDispatch!=undefined) {
        console.log(this.geofenceState[i].name)
        // si esta adentro, verificar el estado
        if (this.geofenceState[i].state==0 && (this.geofenceState[i].geo_type=="plant" || this.geofenceState[i].geo_type==1)) {
          //si el estado es 0, entonces cambiamos el estado de la aplicacion general para mostrar 
          //el boton de carga
          this.stepNumber=1;
          this.geofenceState[i].state=1;
        } else if (this.geofenceState[i].state==0 && (this.geofenceState[i].geo_type=="jobsite" || this.geofenceState[i].geo_type==2)) {
          //si el estado es 0, entonces cambiamos el estado de la aplicacion general para mostrar 
          //el boton de carga
          this.stepNumber=3;
          this.geofenceState[i].state=3;
        }
      } else {
        //sino, revisar si ya salio de la geofence para desactivar los botones
        if (this.geofenceState[i].state!=0) {
          this.geofenceState[i].state=0;
          this.stepNumber=0;
        }
      }
    }
  }
  gotResponseFromGPS(data){

  }
  cancelOrder(){
    // status para probar prove of delivery
    this.stepNumber=3;
    this.statusState=9;
    let confirm = Alert.create({
      title: 'Cancelar orden',
      message: 'Estas a punto de cancelar la orden, ¿Deseas realmente cancelar la orden?',
      buttons: [
        {
          text: 'No cancelar',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Sí cancelar',
          handler: () => {
            this.reallyCancel();
          }
        }
      ]
    });
    this.nav.present(confirm);
  }
  reallyCancel(){
    this._directionsRenderer.set('directions', null);
    window.GPSPosition=undefined;
      var usercoin=new Object();
      usercoin.order=window.currentOrderInDispatch.id;
      usercoin.authenticity_token=window.TOKEN;
      usercoin.utf8="✓"
      var body = JSON.stringify(usercoin);
      var headers = new Headers({ 'Content-Type': 'application/json' });
      var options = new RequestOptions({ headers: headers });
      window.this.http.post(SERVER_URL+'/order/cancel.json',body , options)
      .subscribe(
        data=> window.this.gotResponseFromGPS(data),
        err => window.this.logError(err.json().message),
        () => console.log('cancel Complete')
      );
      window.currentOrderInDispatch=undefined;
  }
  goBackToCenter(){

  }
  callNumber(){
    window.location="tel:"+this.telephone;
  }
  lookingDriverEventHandler(data){
    if(window.thisOS!="macintel"){
      cordova.plugins.notification.local.schedule({
      title: "Orden entrante",
      message: "La orden "+data.order+" requiere un conductor."
    });  
    }
    
    if(data.telephone!=null && data.telephone!=undefined){
      this.telephone=data.telephone;
    }
    this.modal = Modal.create(PromptForOrder,{ order: data.order,timeout:data.timeout,points:data.points,direccion:data.address });
    this.nav.present(this.modal);
    //setTimeout(() => window.closePromptRequest(), data.timeout*1000);
  }
  closePromptRequest(){
    window.controller.dismiss({});
  }
  addMarker(){
	  var marker = new google.maps.Marker({
	    map: this.map,
	    animation: google.maps.Animation.DROP,
	    position: this.map.getCenter()
	  });
	  var content = "<h4>Information!</h4>";          
	  this.addInfoWindow(marker, content);
  }
  addInfoWindow(marker, content){

  }
  openRequestWindow(){
  	console.log("loading alert");
    var alert = Alert.create();
    alert.setTitle('Seleccione tipo de recolección');

    alert.addInput({
      type: 'radio',
      label: 'Moto (hasta 5kg)',
      value: 'blue',
      checked: true
    });

    alert.addButton('Cancelar');
    alert.addButton({
      text: 'OK',
      handler: data => {
        this.testRadioOpen = false;
        this.testRadioResult = data;
      }
    });
    this.nav.present(alert).then(() => {
      this.testRadioOpen = true;
    });
  }
  loadMap(){

 	      var minZoomLevel = 16;
        var currentPos=new google.maps.LatLng(25.67593049605625,-100.41711058462803);
        if(window.GPSPosition!=undefined){
          currentPos = new google.maps.LatLng(window.GPSPosition.coords.latitude,window.GPSPosition.coords.longitude);
        }
        this.map = new google.maps.Map(document.getElementById('map'), {
            zoom: minZoomLevel,
            center: currentPos,
            disableDefaultUI: true,
            mapTypeId: google.maps.MapTypeId.ROADMAP
            // styles:window.empakingStyle
        });
        var trafficLayer = new google.maps.TrafficLayer();
        trafficLayer.setMap(this.map);
        // google.maps.event.addDomListener(window, "resize", ()=> {
        //    var center = this.map.getCenter();
        //    google.maps.event.trigger(this.map, "resize");
        //   this.map.setCenter(center); 
        //    console.log("resize map");
        // });
        this._directionsRenderer.setMap(this.map);
        this._directionsRenderer.setOptions({
            //draggable: true
        });
        this.driverMarker = new google.maps.Marker({
          position: currentPos,
          map: this.map,
          title: 'Driver',
          icon:"img/black-marker.png",
          animation: google.maps.Animation.DROP,
        });

        setTimeout(()=>{
          google.maps.event.trigger(this.map, 'resize');
          console.log("outside");
        }, 1000);
        if(NEO==false){
          if (this.currentUser.content.status_driver==3) {
            // esta asignado
            window.masterPosition();
            this.getOrderInProgress();
          }
        } else {
          //window.currentOrderInDispatch={id:8};
          this.getOrderInProgress();
        }
  }

  getRoutePointsAndWaypoints(Points) {
      if (Points.length <= 10) {
          this.drawRoutePointsAndWaypoints(Points);
      }
      else {
          var newPoints = new Array();
          var startPoint = Points.length - 10;
          var Legs = Points.length - 10;
          for (var i = startPoint; i < Points.length; i++) {
              newPoints.push(Points[i]);
          }
          this.drawRoutePointsAndWaypoints(newPoints);
          this.drawPreviousRoute(Legs);
      }

      //un delay para mostrar la ruta general y luego regresar al nivel normal
      setTimeout(() => this.map.setZoom(17), 10000);
      
  }
 
 
  drawRoutePointsAndWaypoints(Points) {
      //Define a variable for waypoints.
      var _waypoints = new Array();
   
      if (Points.length > 2) //Waypoints will be come.
      {
          for (var j = 1; j < Points.length - 1; j++) {
              var address = Points[j];
              if (address !== "") {
                  _waypoints.push({
                      location: address,
                      stopover: true  //stopover is used to show marker on map for waypoints
                  });
              }
          }
          //Call a drawRoute() function
          this.drawRoute(Points[0], Points[Points.length - 1], _waypoints);
      } else if (Points.length > 1) {
          //Call a drawRoute() function only for start and end locations
          this.drawRoute(Points[this._mapPoints.length - 2], Points[Points.length - 1], _waypoints);
      } else {
          //Call a drawRoute() function only for one point as start and end locations.
          this.drawRoute(Points[this._mapPoints.length - 1], Points[Points.length - 1], _waypoints);
      }
  }

  drawRoute(originAddress, destinationAddress, _waypoints) {
      //Define a request variable for route .
      var _request = '';
   
      //This is for more then two locatins
      if (_waypoints.length > 0) {
          _request = {
              origin: originAddress,
              destination: destinationAddress,
              waypoints: _waypoints, //an array of waypoints
              optimizeWaypoints: true, //set to true if you want google to determine the shortest route or false to use the order specified.
              travelMode: google.maps.DirectionsTravelMode.DRIVING
          };
      } else {
          //This is for one or two locations. Here noway point is used.
          _request = {
              origin: originAddress,
              destination: destinationAddress,
              travelMode: google.maps.DirectionsTravelMode.DRIVING
          };
      }
   
      //This will take the request and draw the route and return response and status as output
      this.directionsService.route(_request, (_response,_status)=> {
          if (_status == google.maps.DirectionsStatus.OK) {
            window.driverScreenjs._directionsRenderer.setDirections(_response);
          }
      });
  }

  requestEPOD(){
    this.modal2 = Modal.create(PromptForSignature,{  });
    this.nav.present(this.modal2);
  }

  getETA(){
    
    
      if (window.currentOrderInDispatch==undefined && window.currentOrderInDispatch==null) {
        this.titleOrETA="Servicios";
        return;
      }
      var storage = window.localStorage;
      var mroute = storage.getItem("route");
      var mforigin=window.GPSPosition.coords.latitude+","+window.GPSPosition.coords.longitude;
      var mfdestination=null;
      if (window.currentOrderInDispatch.points==null || window.currentOrderInDispatch.points=="null" || window.currentOrderInDispatch.points==undefined) {
        return;
      }
      if (this.statusState<7) {
        mfdestination=JSON.parse(window.currentOrderInDispatch.points)[1].lat+","+JSON.parse(window.currentOrderInDispatch.points)[1].lng;
      } else {
        mfdestination=JSON.parse(window.currentOrderInDispatch.points)[2].lat+","+JSON.parse(window.currentOrderInDispatch.points)[2].lng;
      }
      this.http.get("https://maps.googleapis.com/maps/api/distancematrix/json?origins="+mforigin+"&destinations="+mfdestination+"&key=AIzaSyBwdKhkBS0VwUhiX8beHePaHmjV6WIuVTs")
      .subscribe(
        data=>this.gotETA(data),
        err => this.logError(err.json().message),
        () => console.log('gps Complete')
    );

  }
  gotETA(data){
    console.log(data)
    var travelInformation=JSON.parse(data._body);
    if(window.currentOrderInDispatch==null || window.currentOrderInDispatch==undefined){
      this.titleOrETA="Servicios";
      return;
    }
    this.titleOrETA=travelInformation.rows[0].elements[0].duration.text+" "+travelInformation.rows[0].elements[0].distance.text;
  }

}
