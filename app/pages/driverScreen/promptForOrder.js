import {Page,Alert,NavController,NavParams,Modal,ViewController} from 'ionic-angular';
import { Inject } from '@angular/core';
import {Http, RequestOptions,Headers} from '@angular/http';
import {SERVER_URL} from '../../services/server-config'
import {NEO} from '../../services/server-config';
@Page({
  templateUrl: 'build/pages/driverScreen/promptForOrder.html'
})
export class PromptForOrder {
  onPageLoaded() {
    console.log("I'm alive!");
    
  }
  static get parameters() {
    return [[NavController], [NavParams],[Http],[RequestOptions],[ViewController]];
  }
  constructor(nav,navParams,http,requestOptions,controller) {
    this.nav=nav;
    this.http=http;
    this.navParams=navParams;
    this.currentUser=window.user_session;
    this.controller=controller;
    this.orderStart="";
    this.orderEnd="";
    this.direccion=navParams.data.direccion;
    setTimeout(() => this.close(), Number(navParams.data.timeout)*1000);
    window.byPassTime=false;
  }
  close() {
    if(window.byPassTime==false){
      this.controller.dismiss();
      var usercoin=new Object();
      usercoin.authenticity_token=window.TOKEN;
      usercoin.utf8="✓"
      usercoin.order=this.navParams.get('order');
      usercoin.ignored=window.user_session.content.id
      var body = JSON.stringify(usercoin);
      var headers = new Headers({ 'Content-Type': 'application/json' });
      var options = new RequestOptions({ headers: headers });
      this.http.post(SERVER_URL+'/request/driver.json',body , options)
      .subscribe(
        data=>this.gotResponseFromRequest(data),
        err => this.logError(err.json().message),
        () => console.log('Authentication Complete')
      );
    }
    window.byPassTime=false;
  }
  logError(data){
    console.log(data)
  }

  gotResponseFromRequest(data){
    console.log(data);
  }

  acceptIncomingOrder(){
    this.controller.dismiss();
    window.byPassTime=true;
    var usercoin=new Object();
    usercoin.authenticity_token=window.TOKEN;
    usercoin.utf8="✓"
    usercoin.order=this.navParams.get('order');
    usercoin.truck=this.currentUser.content.id;
    usercoin.decision=true;
    var body = JSON.stringify(usercoin);
    var headers = new Headers({ 'Content-Type': 'application/json' });
    var options = new RequestOptions({ headers: headers });
    if (NEO==false) {
      this.http.post(SERVER_URL+'/request/accept.json',body , options)
    .subscribe(
      data=>this.gotAcceptedOrder(data),
      err => this.logError(err.json().message),
      () => console.log('Authentication Complete')
    );
    } else {
      this.http.post(SERVER_URL+'/TruckFinder/AcceptOrder?order='+usercoin.order+'&truck='+usercoin.truck+'&decision='+usercoin.decision,body , options)
    .subscribe(
      data=>this.gotAcceptedOrder(data),
      err => this.logError(err.json().message),
      () => console.log('Authentication Complete')
    );
    }
    
  }

  gotAcceptedOrder(data){
    window.currentOrderInDispatch=JSON.parse(data._body);
    
    if(window.masterGPSTimeout==undefined){
      window.driverScreenjs.captureGPS();
      window.masterGPSTimeout=1;
    }

    window.driverScreenjs.startProcessingRoute(window.currentOrderInDispatch.points);
  }

  generateImagePoints(){
    var datos="dsdf";
    //new google.maps.LatLng(window.GPSPosition.coords.latitude,window.GPSPosition.coords.longitude);
//     https://maps.googleapis.com/maps/api/staticmap?center=Brooklyn+Bridge,New+York,NY&zoom=13&size=600x300&maptype=roadmap
// &markers=color:blue%7Clabel:S%7C40.702147,-74.015794&markers=color:green%7Clabel:G%7C40.711614,-74.012318
// &markers=color:red%7Clabel:C%7C40.718217,-73.998284
// &key=AIzaSyBwdKhkBS0VwUhiX8beHePaHmjV6WIuVTs
    
    //var initialSection="https://maps.googleapis.com/maps/api/staticmap?center="+window.GPSPosition.coords.latitude+","+window.GPSPosition.coords.longitude+"&size=600x300&maptype=roadmap";
    var initialSection="https://maps.googleapis.com/maps/api/staticmap?size=600x300&maptype=roadmap";
    var marcadores="";
    //var sdata=data.replace(/'/g,'"');
    var inm=JSON.parse(this.navParams.get('points'));
    var colores=["blue","red","green","purple","yellow","orange","pink"];
    for (var i = inm.length - 1; i >= 0; i--) {
      this.getAddressName(inm[i].lat+","+inm[i].lng);
      marcadores=marcadores+"&markers=color:"+colores[i]+"%7Clabel:S%7C"+inm[i].lat+","+inm[i].lng
    }
  marcadores=marcadores+"&key=AIzaSyBwdKhkBS0VwUhiX8beHePaHmjV6WIuVTs";
  return initialSection+marcadores
  return "";
  }

  getAddressName(punto){
    //https://maps.googleapis.com/maps/api/geocode/json?latlng=40.714224,-73.961452&key=YOUR_API_KEY
    // this.http.get('https://maps.googleapis.com/maps/api/geocode/json?latlng='+punto+"&key=AIzaSyBwdKhkBS0VwUhiX8beHePaHmjV6WIuVTs")
    //   .subscribe(
    //     data=>this.gotAddress(data),
    //     err => this.logError(err.json().message),
    //     () => console.log('getAddresName()')
    // );
  }

  gotAddress(data){
    if(this.orderStart==""){

    } else {

    }
  }
  

}
