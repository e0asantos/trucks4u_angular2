import {Page,Alert,NavController,NavParams,Modal,ViewController} from 'ionic-angular';
import { Inject } from '@angular/core';
import {Http, RequestOptions,Headers} from '@angular/http';
import {SERVER_URL} from '../../services/server-config';
import {NEO} from '../../services/server-config';
@Page({
  templateUrl: 'build/pages/driverScreen/promptForSignature.html'
})
export class PromptForSignature {
  onPageLoaded() {
    console.log("promptForSignature");
    
  }
  ngAfterViewInit() {
    console.log("ngAfterViewInit")

    this.canvas = document.getElementById('signatureCanvas');
    var canvas2 = document.getElementById("elementoPrueba");
    this.signaturePad = new SignaturePad(this.canvas);
    window.onresize = this.resizeCanvas;
    this.resizeCanvas();
  }
  resizeCanvas() {
    // When zoomed out to less than 100%, for some very strange reason,
    // some browsers report devicePixelRatio as less than 1
    // and only part of the canvas is cleared then.
    var ratio =  Math.max(window.devicePixelRatio || 1, 1);
    this.canvas.width = this.canvas.offsetWidth * ratio;
    this.canvas.height = this.canvas.offsetHeight * ratio;
    this.canvas.getContext("2d").scale(ratio, ratio);
}



  static get parameters() {
    return [[NavController], [NavParams],[Http],[RequestOptions],[ViewController]];
  }
  constructor(nav,navParams,http,requestOptions,controller) {
    this.nav=nav;
    this.http=http;
    this.navParams=navParams;
    this.currentUser=window.user_session;
    this.controller=controller;
    this.canvas=null;
    //setTimeout(() => this.close(), Number(navParams.data.timeout)*1000);
    //window.byPassTime=false;
    
    console.log("onConstructor")
    
  }
  completeJob(){
    if (this.signaturePad.isEmpty()) {
      let alert = Alert.create({
        title: "Información",
        subTitle: "No se ha detectado una firma de entrega.",
        buttons: ['OK']
      });
      this.nav.present(alert);
      return;
    }
    window.onresize=null;
    window.GPSPosition=undefined;
      var usercoin=new Object();
      

      usercoin.authenticity_token=window.TOKEN;
      if (NEO==true) {
        usercoin.OrderId=window.currentOrderInDispatch.id;
        usercoin.Image=this.signaturePad.toDataURL().substr(22);  
        usercoin.truck=window.user_session.content.id
      } else {
        usercoin.prove=this.signaturePad.toDataURL();  
        usercoin.order=window.currentOrderInDispatch.id;
      }
      
      usercoin.utf8="✓"
      var body = JSON.stringify(usercoin);
      var headers = new Headers({ 'Content-Type': 'application/json' });
      var options = new RequestOptions({ headers: headers });
      if (NEO==true) {
        
        window.this.http.post(SERVER_URL+'/TruckFinder/SaveImage',body , options)
        .subscribe(
          data=> console.log("finish job"),
          err => window.this.logError(err.json().message),
          () => console.log('finish Complete')
        );
      } else {
        window.this.http.post(SERVER_URL+'/order/completed.json',body , options)
        .subscribe(
          data=> console.log("finish job"),
          err => window.this.logError(err.json().message),
          () => console.log('finish Complete')
        );
      }
      window.currentOrderInDispatch=undefined;
      window.driverScreenjs._directionsRenderer.set('directions', null);
      this.close();
  }
  clearCanvas(){
    this.signaturePad.clear();
  }
  close() {
    window.onresize=null;
    // if(window.byPassTime==false){
      this.controller.dismiss();
    //   var usercoin=new Object();
    //   usercoin.authenticity_token=window.TOKEN;
    //   usercoin.utf8="✓"
    //   usercoin.order=this.navParams.get('order');
    //   var body = JSON.stringify(usercoin);
    //   var headers = new Headers({ 'Content-Type': 'application/json' });
    //   var options = new RequestOptions({ headers: headers });
    //   this.http.post(SERVER_URL+'/request/driver.json',body , options)
    //   .subscribe(
    //     data=>this.gotResponseFromRequest(data),
    //     err => this.logError(err.json().message),
    //     () => console.log('Authentication Complete')
    //   );
    // }
    // window.byPassTime=false;
  }

  gotResponseFromRequest(data){
    console.log(data);
  }

  
  

}
