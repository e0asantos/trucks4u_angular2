import {Page,Alert,NavController,NavParams,Modal,ViewController} from 'ionic-angular';
import { Inject } from '@angular/core';
import {Http, RequestOptions,Headers} from '@angular/http';
import {SERVER_URL} from '../../services/server-config';
import {DriverScreen} from '../driverScreen/driverScreen';
import {NEO} from '../../services/server-config';


@Page({
  templateUrl: 'build/pages/historyOrder/historyOrder.html'
})
export class HistoryOrder {
  static get parameters() {
    return [[NavController],[Http],[NavParams]];
  }

  constructor(controller,http,params) {
  	this.controller=controller;
  	this.nav=params;
  	this.http=http;
  	this.currentUser=window.user_session;
  	this.getHistoryOrder();
  	this.fullHistory=[];
  	this.usercoin=new Object();
  	
  }

  isAvailableToStart(statusID){
  	if ((statusID==7 || statusID==9) && window.user_session.content.is_driver==1) {
  		return true;
  	} else {
  		return false;
  	}
  }

  acceptIncomingOrder(orderID){
    
    var usercoin=new Object();
    usercoin.authenticity_token=window.TOKEN;
    usercoin.utf8="✓"
    usercoin.order=orderID;
    usercoin.truck=this.currentUser.content.id;
    usercoin.decision=true;
    var body = JSON.stringify(usercoin);
    var headers = new Headers({ 'Content-Type': 'application/json' });
    var options = new RequestOptions({ headers: headers });
    if (NEO==false) {
      this.http.post(SERVER_URL+'/request/accept.json',body , options)
    .subscribe(
      data=>this.gotAcceptedOrder(data),
      err => this.logError(err.json().message),
      () => console.log('aceptar completado')
    );
    } else {
      this.http.post(SERVER_URL+'/TruckFinder/AcceptOrder?order='+usercoin.order+'&truck='+usercoin.truck+'&decision='+usercoin.decision,body , options)
    .subscribe(
      data=>this.gotAcceptedOrder(data),
      err => this.logError(err.json().message),
      () => console.log('Authentication Complete')
    );
    }
    
  }

  gotAcceptedOrder(data){
  	console.log(data);
  	var preOrder=JSON.parse(data._body);
  	if (preOrder=="driver_busy") {
  		let alert = Alert.create({
	      title: 'Error al asignar',
	      subTitle: 'Al parecer tienes una orden en proceso anterior, no se puede asignar una orden nueva.',
	      buttons: ['OK']
	    });
	    this.controller.present(alert);
  		return;
  	} else {
      //this.controller.setRoot(DriverScreen);
      let alert1 = Alert.create({
        title: 'Pedido iniciado',
        subTitle: 'El pedido se ha marcado como iniciado, ve al menu -En proceso- para visualizar la ruta.',
        buttons: ['OK']
      });
      this.controller.present(alert1);
    }
    window.currentOrderInDispatch=JSON.parse(data._body);
    
    if(window.masterGPSTimeout==undefined){
      window.driverScreenjs.captureGPS();
      window.masterGPSTimeout=1;
    }
    this.getHistoryOrder();

    //window.driverScreenjs.startProcessingRoute(window.currentOrderInDispatch.points);
  }

  getHistoryOrder(){
  	if (NEO==true) {
  		this.http.get(SERVER_URL+'/TruckFinder/OrdersByTruckId/'+this.currentUser.content.id)
	      .subscribe(
	        data=>this.gotHistoryOrder(data),
	        err => this.logError(err.json().message),
	        () => console.log('gps Complete')
	    ); 
  	} else {
  		if (this.usercoin==null || this.usercoin==undefined) {
  			this.usercoin=new Object();
  		}
  		this.usercoin.authenticity_token=window.TOKEN;
    	this.usercoin.utf8="✓"
  		var body = JSON.stringify(this.usercoin);
	    var headers = new Headers({ 'Content-Type': 'application/json' });
	    var options = new RequestOptions({ headers: headers });
	    this.http.post(SERVER_URL+'/order/history.json',body , options)
	    	.subscribe(
	        data=>this.gotHistoryOrder2(data),
	        err => this.logError(err.json().message),
	        () => console.log('history Complete')
	      );
  	}
  }

  gotHistoryOrder(data){
  	var arr=JSON.parse(data._body).Table;
  	for (var i = arr.length - 1; i >= 0; i--) {
  		arr[i]
  	}
  	this.fullHistory=arr;
  }
  gotHistoryOrder2(data){
  	console.log(data)
  	var arr=JSON.parse(data._body).reverse();
  	for (var i = arr.length - 1; i >= 0; i--) {
  		arr[i]
  	}
  	this.fullHistory=arr;
  }

  logError(msg){

  }
  generateImagePoints(data){
  	var datos="dsdf";
  	//new google.maps.LatLng(window.GPSPosition.coords.latitude,window.GPSPosition.coords.longitude);
  	//var initialSection="https://maps.googleapis.com/maps/api/staticmap?center="+window.GPSPosition.coords.latitude+","+window.GPSPosition.coords.longitude+"&size=600x300&maptype=roadmap";
  	var initialSection="https://maps.googleapis.com/maps/api/staticmap?size=600x300&maptype=roadmap&zoom=14";
  	var marcadores="";
  	var sdata=data.replace(/'/g,'"');

  	var inm=[];
  	if(data.indexOf("[")==-1){
  		inm=JSON.parse("["+sdata+"]");
  	} else {
  		inm=JSON.parse(sdata);
  	}
  	var colores=["blue","red","green","purple","yellow","orange","pink"];
  	for (var i = inm.length - 1; i >= 0; i--) {
  		marcadores=marcadores+"&markers=color:"+colores[i]+"%7Clabel:S%7C"+inm[i].lat+","+inm[i].lng
  	}
	marcadores=marcadores+"&key=AIzaSyBwdKhkBS0VwUhiX8beHePaHmjV6WIuVTs";
	return initialSection+marcadores
	//return "";
  }

}
