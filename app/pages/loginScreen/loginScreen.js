import {Page,NavController,Alert,NavParams,Loading} from 'ionic-angular';
import {Http, RequestOptions,Headers} from '@angular/http';
import {RequesterScreen} from '../requesterScreen/requesterScreen';
import {DriverScreen} from '../driverScreen/driverScreen';
import {RegistrationScreenPage} from '../registration-screen/registration-screen';
import {SERVER_URL} from '../../services/server-config';
import {NEO} from '../../services/server-config';
import {user_session} from '../../services/server-config'
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';
import {Injectable} from '@angular/core';

//url: window.ENDPOINT+'/usuarios/createToken.json'
@Page({
  templateUrl: 'build/pages/loginScreen/loginScreen.html'
})
@Injectable()
export class LoginScreen {
  
  static get parameters(){
  	return [[Http],[RequestOptions],[NavController],[NavParams]];
  }
  constructor(http,requestOptions,nav) {
  	this.http=http;
  	this.usercoin=new Object();
  	this.nav=nav;
    this.loadingDialog=new Object();
    if (NEO==false) {
  	 this.getTokenCommunication();
  	 this.getPackagesAvailableFromServer();
    }
    var storage = window.localStorage;
    if (storage.getItem("internalu")!=null && storage.getItem("internalu")!=undefined) {
      this.usercoin.userEmail=storage.getItem("internalu");
      this.usercoin.userPwd=storage.getItem("internalp");
      //this.loginProcess();
    }
     
  	
  }
  getPackagesAvailableFromServer(){
  	var body = JSON.stringify({});
    var headers = new Headers({ 'Content-Type': 'application/json' });
    var options = new RequestOptions({ headers: headers });
  	this.http.get(SERVER_URL+'/vehicle_types.json')
  	.subscribe(
      data=>this.gotResponseFromPackages(data),
      err => this.logError(err.json().message),
      () => console.log('Authentication Complete')
    );
  }
  getTokenCommunication(){
  	
    var headers = new Headers({ 'Content-Type': 'application/json' });
    var options = new RequestOptions({ headers: headers });
  	this.http.get(SERVER_URL+'/usuarios/createToken.json')
  	.subscribe(
      data=>this.gotTokenFromServer(data),
      err => this.logError(err.json().message),
      () => console.log('Authentication Complete')
    );
  }
  gotoRegistration(){
    this.nav.push(RegistrationScreenPage, {});
  }
  gotTokenFromServer(response){
  	window.TOKEN=JSON.parse(response._body).token;
  }
  gotResponseFromPackages(response){
  	window.packagesAvailable=JSON.parse(response._body);
  }
  loginProcess(){
    if (this.usercoin.userEmail==""
      || this.usercoin.userEmail==null
      || this.usercoin.userEmail==undefined
      || this.usercoin.userPwd==""
      || this.usercoin.userPwd==null
      || this.usercoin.userPwd==undefined) {
      let alert = Alert.create({
        title: "Error",
        subTitle: "Complete los campos de usuario y contraseña para continuar.",
        buttons: ['OK']
      });
    this.nav.present(alert);
      return;
    }
    
    this.loadingDialog = Loading.create({
      content: 'Iniciando sesión...'
    });
    this.nav.present(this.loadingDialog);
    this.usercoin.authenticity_token=window.TOKEN;
    this.usercoin.utf8="✓"
    var body = JSON.stringify(this.usercoin);
    var headers = new Headers({ 'Content-Type': 'application/json' });
    var options = new RequestOptions({ headers: headers });
    if (NEO==false) {

    	this.http.post(SERVER_URL+'/usuarios/loginMobile.json',body , options)
    	.subscribe(
        data=>this.gotResponseFromLogin(data),
        err => this.logError(err.json().message),
        () => console.log('Authentication Complete')
      );
    } else {
      var usercoin1=new Object();
      usercoin1.grant_type="password"
      usercoin1.username="SuperUser"
      usercoin1.password="neoris00"
      // var body = JSON.stringify(usercoin1);
      // var headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
      // var options = new RequestOptions({ headers: headers });
      // this.http.post(SERVER_URL+'/token',"grant_type=password&username="+this.usercoin.userEmail+"&password="+this.usercoin.userPwd , options)
      // .subscribe(
      //   data=>this.gotResponseFromNeoLogin(data),
      //   err => this.logError(err.json().message),
      //   () => console.log('Authentication Complete')
      // );
      this.http.post(SERVER_URL+'/TruckFinder/ValidateUser?username='+this.usercoin.userEmail+'&password='+this.usercoin.userPwd,body , options)
      .subscribe(
        data=>this.gotResponseFromLogin(data),
        err => this.logError(err.json().message),
        () => console.log('Authentication Complete')
      );
      // var response=new Object();
      // response["_body"]=JSON.stringify({content:{is_driver:1,name:"Driver",lastname:"Driver",id:2,operativ:"online"},access_token:2});

      // this.gotResponseFromLogin(response);
    }
    
  }
  logError(msg){
  	console.log(msg);
  }
  gotResponseFromLogin(response){
    console.log(response);
	   this.loadingDialog.dismiss();
    if (NEO==true) {
      if (response._body==null || response._body=="null") {
         let alert = Alert.create({
            title: "Error",
            subTitle: "Nombre de usuario o contraseña incorrectos.",
            buttons: ['OK']
          });
          this.nav.present(alert);
          return;
      }
      var usuario=JSON.parse(response._body).Table;
      if (usuario[0].Driver==0) {
         let alert = Alert.create({
        title: "Error",
        subTitle: "Nombre de usuario o contraseña incorrectos.",
        buttons: ['OK']
      });
      this.nav.present(alert);
      } else {
        
        // add the token device to the user
        // $http({ 
        //   method: 'POST',
        //   url: window.ENDPOINT+'/push/update.json',
        //   data:{authenticity_token:window.token,token:window.pushToken,device_id:window.pushTokenDevice}
        // }).then(function successCallback(response) {
        //   console.log(response);
        // }, function errorCallback(response) {
        //   //$Alert.error("Error","Ocurrió un error al intentar descargar georegiones.");
        //   console.log(response);
        // });
        var storage = window.localStorage;
        storage.setItem("internalu", this.usercoin.userEmail);
        storage.setItem("internalp", this.usercoin.userPwd);
        storage.setItem("welcome1",true);
        window.thisAppjs.menuConfigurator("driver");
        window.user_session={content:{is_driver:1,name:"Juan",lastname:"Lopez",id:usuario[0].Driver,operativ:"online"},access_token:usuario[0].Driver};
        this.nav.setRoot(DriverScreen);
      }
    } else {
      window.user_session=JSON.parse(response._body);
    if (window.user_session.type=="error") { 
      // $Alert.error("Error",response.data.content);
      let alert = Alert.create({
	      title: window.user_session.type,
	      subTitle: window.user_session.content,
	      buttons: ['OK']
	    });
	  this.nav.present(alert);
    } else {
      // var infoPush=new Object();
      // infoPush.token=window.pushToken;
      // infoPush.device_id=window.pushTokenDevice;
      // infoPush.authenticity_token=window.TOKEN;
      // infoPush.utf8="✓"
      // var body = JSON.stringify(infoPush);
      // var headers = new Headers({ 'Content-Type': 'application/json' });
      // var options = new RequestOptions({ headers: headers });
      // this.http.post(SERVER_URL+'/push/update.json',body , options)
      // .subscribe(
      //   data=>this.gotResponseFromPush(data),
      //   err => this.logError(err.json().message),
      //   () => console.log('Push token sent')
      // );
      var storage = window.localStorage;
      storage.setItem("internalu", this.usercoin.userEmail);
      storage.setItem("internalp", this.usercoin.userPwd);
      storage.setItem("welcome1",true);
  	  if (window.user_session.content.is_driver==0) {
  	  	//gotorequest
        window.thisAppjs.menuConfigurator("user");
  	  	this.nav.setRoot(RequesterScreen);
  	  } else {
        window.thisAppjs.menuConfigurator("driver");
  	  	this.nav.setRoot(DriverScreen);
  	  }
    }
    }
  }
  gotResponseFromPush(response){

  }
  gotResponseFromNeoLogin(response){
    console.log(response);
    window.user_session=JSON.parse(response._body);
    var algo=1;
    if (window.user_session.type=="error") { 
      // $Alert.error("Error",response.data.content);
      let alert = Alert.create({
        title: window.user_session.type,
        subTitle: window.user_session.content,
        buttons: ['OK']
      });
    this.nav.present(alert);
    } else {
    if (window.user_session.access_token!=undefined) {
      //gotorequest
      window.TOKEN=window.user_session.access_token;
      this.nav.setRoot(DriverScreen);
    } else {
      this.nav.setRoot(RequesterScreen);
    }
    }
  }
  handleError(error) {
        console.log(error);
        //return Observable.throw(error.json().error || 'Server error');
    }

}
