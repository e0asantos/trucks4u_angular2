import {Page, NavController} from 'ionic-angular';
import {LoginScreen} from '../loginScreen/loginScreen';
/*
  Generated class for the LogoutPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Page({
  templateUrl: 'build/pages/logout/logout.html',
})
export class LogoutPage {
  static get parameters() {
    return [[NavController]];
  }

  constructor(nav) {
    this.nav = nav;
    //borrar los valores de orden
    var storage = window.localStorage;
    storage.setItem("internalu", null);
    storage.setItem("internalp", null);
    window.masterCheckList=undefined;
    window.user_session=undefined;
    window.currentOrderInDispatch=undefined;
    this.nav.setRoot(LoginScreen);
  }
}
