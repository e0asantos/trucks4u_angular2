import {Page,NavController,Alert,NavParams,Loading} from 'ionic-angular';
import {Http, RequestOptions,Headers} from '@angular/http';
import {RequesterScreen} from '../requesterScreen/requesterScreen';
import {DriverScreen} from '../driverScreen/driverScreen';
import {SERVER_URL} from '../../services/server-config';
import {NEO} from '../../services/server-config';
import {user_session} from '../../services/server-config'
import {LoginScreen} from '../loginScreen/loginScreen';

/*
  Generated class for the RegistrationScreenPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Page({
  templateUrl: 'build/pages/registration-screen/registration-screen.html',
})
export class RegistrationScreenPage {
  static get parameters() {
    return [[NavController],[Http]];
  }

  constructor(nav,http) {
    this.nav = nav;
    this.http=http;
    this.loadingDialog=new Object();
    window.registrationScreenjs=this;
    this.usercoin=new Object();
  }
  reUpdateExpiration(){
    setTimeout(() => {
      this.getMonthOnly();
      this.getYearOnly();
    }, 1000);
    
  }
  success_callbak(data){
    console.log(data);
    window.registrationScreenjs.usercoin.deviceToken=data.data.id;
    window.registrationScreenjs.attemptRegistration();
  }
  error_callbak(data){
    console.log(data);
    let alert = Alert.create({
          title: 'Error',
          subTitle: data.data.description,
          buttons: ['OK']
        });
    window.registrationScreenjs.nav.present(alert);
  }
  getMonthOnly(){
    if (this.usercoin.cardexpire!=undefined && this.usercoin.cardexpire!=null) {
      this.usercoin.expiremonth=this.usercoin.cardexpire.split("-")[1];
    }
  }
  getYearOnly(){
    if (this.usercoin.cardexpire!=undefined && this.usercoin.cardexpire!=null) {
      this.usercoin.expireyear=this.usercoin.cardexpire.split("-")[0].substr(2);
    }
    
  }
  preAttemptUpdate(){
    if (this.usercoin.card!=undefined && this.usercoin.card!=""
      && this.usercoin.cardexpire!=undefined && this.usercoin.cardexpire!=""
       && this.usercoin.ccv!=undefined && this.usercoin.ccv!="" ) {
        this.reUpdateExpiration();
        OpenPay.token.extractFormAndCreate('customer-form', this.success_callbak, this.error_callbak);
    } else {
      this.attemptRegistration();
    }
  }
  attemptRegistration(){
    
    if (this.usercoin.name!=undefined && this.usercoin.name!=""
      && this.usercoin.lastname!=undefined && this.usercoin.lastname!=""
      && this.usercoin.mobile!=undefined && this.usercoin.mobile!=""
      && this.usercoin.email!=undefined && this.usercoin.email!=""
      && this.usercoin.hashseed!=undefined && this.usercoin.hashseed!=""
      && this.usercoin.birthday!=undefined && this.usercoin.birthday!=""
      // && this.usercoin.card!=undefined && this.usercoin.card!=""
      // && this.usercoin.cardexpire!=undefined && this.usercoin.cardexpire!=""
      // && this.usercoin.ccv!=undefined && this.usercoin.ccv!="" 
      ) {
        this.loadingDialog = Loading.create({
          content: 'Registrando...'
        });
        this.nav.present(this.loadingDialog);
        this.usercoin.authenticity_token=window.TOKEN;
        var body = JSON.stringify(this.usercoin);
        var headers = new Headers({ 'Content-Type': 'application/json' });
        var options = new RequestOptions({ headers: headers });
        this.http.post(SERVER_URL+'/usuarios/createMobile.json',body , options)
        .subscribe(
          data=>this.gotResponseFromRegistration(data),
          err => this.logError(err.json().message),
          () => console.log('order placed')
        );
        this.stepNumber=0;
        } else {
          let alert = Alert.create({
          title: 'Error',
          subTitle: 'Antes de intentar registrarte, verifica que la información este completa',
          buttons: ['OK']
        });
      this.nav.present(alert);
    }
    // this.http.post(SERVER_URL+'/order/place.json',body , options)
    // .subscribe(
    //   data=>this.gotResponseFromOrderRequest(data),
    //   err => this.logError(err.json().message),
    //   () => console.log('order placed')
    // );
  }
  gotResponseFromRegistration(data){
    this.loadingDialog.dismiss();
    this.realInformation=JSON.parse(data._body);
    if (this.realInformation.msg_type=="error") {
      let alert = Alert.create({
          title: 'Error',
          subTitle: this.realInformation.msg_content,
          buttons: ['OK']
        });
      this.nav.present(alert);
    } else {
      let alert = Alert.create({
          title: 'Bienvenido',
          subTitle: this.realInformation.msg_content,
          buttons: [{text:'OK',handler: () => {
      // user has clicked the alert button
      // begin the alert's dimiss transition
      let navTransition = alert.dismiss();

      // start some async method
      this.nav.setRoot(LoginScreen);
      return false;
    }}]
        });
      this.nav.present(alert);
    }
  }

}
