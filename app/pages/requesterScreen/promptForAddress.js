import {Page,Alert,NavController,NavParams,Modal,ViewController} from 'ionic-angular';
import { Inject } from '@angular/core';
import {Http, RequestOptions,Headers} from '@angular/http';
import {SERVER_URL} from '../../services/server-config'
@Page({
  templateUrl: 'build/pages/requesterScreen/promptForAddress.html'
})
export class PromptForAddress {
  onPageLoaded() {
    console.log("I'm alive!");
    
  }
  static get parameters() {
    return [[NavController], [NavParams],[Http],[RequestOptions],[ViewController]];
  }
  constructor(nav,navParams,http,requestOptions,controller) {
    this.nav=nav;
    this.http=http;
    this.navParams=navParams;
    this.currentUser=window.user_session;
    this.controller=controller;
    this.usercoin=new Object();
    this.stepNumber=0;
  }
  close() {
    
      this.controller.dismiss();
      
  }

 

  attemptRecollection(){
    this.stepNumber=1;
    this.usercoin
    this.usercoin.authenticity_token=window.TOKEN;
    this.usercoin.utf8="✓"
    var mainParent=this.navParams.get('mainParent');
    this.usercoin.vehicle_type_id=mainParent.preOrderToRequest.vehicle_type_id
    this.usercoin.origin=mainParent.preOrderToRequest.origin
    this.usercoin.destination=null;
    var body = JSON.stringify(this.usercoin);
    var headers = new Headers({ 'Content-Type': 'application/json' });
    var options = new RequestOptions({ headers: headers });
    var options = new RequestOptions({ headers: headers });
    this.http.post(SERVER_URL+'/order/place.json',body , options)
    .subscribe(
      data=>this.gotResponseFromOrderRequest(data),
      err => this.logError(err.json().message),
      () => console.log('order placed')
    );
    
  }
  logError(data){
    console.log(data);
  }
  gotResponseFromOrderRequest(data){
    console.log(data);

    var mainParent=this.navParams.get('mainParent');
    mainParent.gotResponseFromOrderRequest(data);
    mainParent.stepNumber=0;
    this.controller.dismiss();
  }

  acceptIncomingOrder(){
    this.controller.dismiss();
    window.byPassTime=true;
    var usercoin=new Object();
    usercoin.authenticity_token=window.TOKEN;
    usercoin.utf8="✓"
    usercoin.order=this.navParams.get('order');
    var body = JSON.stringify(usercoin);
    var headers = new Headers({ 'Content-Type': 'application/json' });
    var options = new RequestOptions({ headers: headers });
    this.http.post(SERVER_URL+'/request/accept.json',body , options)
    .subscribe(
      data=>this.gotAcceptedOrder(data),
      err => this.logError(err.json().message),
      () => console.log('Authentication Complete')
    );
  }

  gotAcceptedOrder(data){
    window.currentOrderInDispatch=JSON.parse(data._body);
    window.captureGPS();
  }


  

}
