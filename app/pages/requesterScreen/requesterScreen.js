import {Page,Alert,NavController,NavParams,Loading,Modal,ViewController} from 'ionic-angular';
import { Inject } from '@angular/core';
import {Http, RequestOptions,Headers} from '@angular/http';
import {SERVER_URL} from '../../services/server-config'
import {PromptForAddress} from './PromptForAddress';
import {NEO} from '../../services/server-config';
import {user_session} from '../../services/server-config'
@Page({
  templateUrl: 'build/pages/requesterScreen/requesterScreen.html'
})
export class RequesterScreen {

  onPageLoaded() {
    console.log("I'm alive user!");
    this.loadMap();
  }

  static get parameters() {
    return [[NavController], [NavParams],[Http],[RequestOptions]];
  }
  constructor(nav,navParams,http,requestOptions,headers) {
    window.this=this;
    window.requesterScreenjs=this;
  	this.nav=nav;
    this.stepNumber=0;
    this.http=http;
    this.preOrderToRequest=new Object();
    this.centerMarker=new Object();
    window.avoidLoading=false;
    this.currentUser=window.user_session;
    this.packagesAvailable=window.packagesAvailable;
    console.log("current URL:"+SERVER_URL+"usuario:");
    console.log(window.user_session);
    Pusher.logToConsole = true;
    var pusher = new Pusher('cb83ffb2e230419bfaeb', {
      encrypted: true
    });
    var channel = pusher.subscribe('client_'+this.currentUser.content.id);
    channel.bind('draw_truck', this.addTrackingMarker);
    channel.bind('no_driver', this.gotNoDriverPush);
    channel.bind('accepted_order', (data)=>{this.gotOrderAccepted(data)});
    channel.bind('order_terminated', (data)=>this.orderTerminated(data));
    this.trackingMarkers=[];



    this.directionsService = new google.maps.DirectionsService();
    this._directionsRenderer = new google.maps.DirectionsRenderer();
    this.titleOrETA="Servicios"


    if (window.thisOS!="macintel") {
      window.notificationOpenedCallback = function(jsonData) {
        console.log('didReceiveRemoteNotificationCallBack: ' + JSON.stringify(jsonData));
      };

    window.plugins.OneSignal.init("e6df78f1-34ff-4041-a31d-c3a62be3fd27",
                                 {googleProjectNumber: "980349758335"},
                                 window.notificationOpenedCallback);
    window.plugins.OneSignal.getIds((ids)=> {
      console.log('getIds: ' + JSON.stringify(ids));
      var infoPush=new Object();
      infoPush.token=ids.pushToken;
      infoPush.device_id=ids.pushTokenDevice;
      infoPush.authenticity_token=window.TOKEN;
      infoPush.utf8="✓"
      var body = JSON.stringify(infoPush);
      var headers = new Headers({ 'Content-Type': 'application/json' });
      var options = new RequestOptions({ headers: headers });
      this.http.post(SERVER_URL+'/push/update.json',body , options)
      .subscribe(
        data=>this.gotResponseFromPush(data),
        err => this.logError(err.json().message),
        () => console.log('Push token sent')
      );
      window.pushToken=ids.pushToken;
      window.pushTokenDevice=ids.userId;
    });
  
   // Show an alert box if a notification comes in when the user is in your app.
   window.plugins.OneSignal.enableInAppAlertNotification(true); 
 }
  }
  gotResponseFromPush(response){

  }
  logError(data){
    console.log(data)
  }
  addTrackingMarker(data){
    var myLatLng = {lat: Number(data.lat), lng: Number(data.lon)};
    
    var marker = new google.maps.Marker({
      position: myLatLng,
      map: window.this.map,
      title: 'Rastreo',
      icon:"img/small-circle.png",
      animation: google.maps.Animation.DROP,
    });
    this.trackingMarkers.push(marker)
  }

  orderTerminated(data){
    window.currentOrderInDispatch=undefined;
    this.statusState=0;
    this.stepNumber=0;
    this.titleOrETA="Servicios";
    this._directionsRenderer.setMap(null);
    for (var i = 0;i<this.trackingMarkers.length;i++) {
      this.trackingMarkers[i].setMap(null);
      this.trackingMarkers[i].setVisible(false); 
    }
    this.trackingMarkers=[];
  }

  cancelRequest(){
    this.stepNumber=0;
  }

  gotOrderAccepted(data){
    if (window.loading!=undefined) {
      window.loading.dismiss();  
    }
    let alert = Alert.create({
      title: 'Recolección en proceso',
      subTitle: 'Si deseas cancelar alguna orden en proceso, puedes hacerlo en el menú ordenes.',
      buttons: ['OK']
    });
    this.nav.present(alert);
    this.getOrderInProgress();
  }
  gotNoDriverPush(data){
    window.avoidLoading=true;
    if (window.loading!=undefined) {
      window.loading.dismiss();  
    }
    let alert = Alert.create({
        title: "Información",
        subTitle: "No encontramos repartidores disponibles, intenta en unos minutos",
        buttons: ['OK']
      });
    window.this.nav.present(alert);
  }
  addMarker(){
	  var marker = new google.maps.Marker({
	    map: this.map,
	    animation: google.maps.Animation.DROP,
	    position: this.map.getCenter()
	  });

	  var content = "<h4>Information!</h4>";          
	  this.addInfoWindow(marker, content);
  }
  addInfoWindow(marker, content){

  }
  openManualAddress(){
    this.modal = Modal.create(PromptForAddress,{mainParent:this});
    this.nav.present(this.modal);
  }
  openRequestWindow(){
  	console.log("loading alert");
    var alert = Alert.create();
    alert.setTitle('Seleccione tipo de recolección');
    // acceso a los paquetes disponibles
    for (var i = this.packagesAvailable.length - 1; i >= 0; i--) {
      
      alert.addInput({
        type: 'radio',
        label: this.packagesAvailable[i].transportation_name+ "("+this.packagesAvailable[i].max_weight+" kg)",
        value: this.packagesAvailable[i].internal_id,
        checked: false
      });
    }
    

    alert.addButton('Cancelar');
    alert.addButton({
      text: 'OK',
      handler: data => this.gotoSecondStep(data)
    });
    this.nav.present(alert).then(() => {
      this.testRadioOpen = true;
    });
  }
  gotoSecondStep(data){
    this.preOrderToRequest.vehicle_type_id=data
    this.preOrderToRequest.utf8="✓"
    this.preOrderToRequest.authenticity_token=window.TOKEN;
    this.stepNumber=1;

  }
  gotoThirdStep(){
    let alert = Alert.create({
      title: 'Agrega un destino',
      subTitle: 'Ya ingresaste el punto de recolección ahora mueve el mapa para seleccionar el destino con la bandera rosa.',
      buttons: ['OK']
    });
    this.nav.present(alert);
    this.preOrderToRequest.origin=this.map.getCenter();
    this.stepNumber=2;
  }
  requestSelectedRecolection(){
    
    this.preOrderToRequest.destination=this.map.getCenter();
    // var requestOrder=new Object();
    // requestOrder.vehicle_type_id=data;
    // requestOrder.utf8="✓"
    // requestOrder.authenticity_token=window.TOKEN;
    var body = JSON.stringify(this.preOrderToRequest);
    var headers = new Headers({ 'Content-Type': 'application/json' });
    //headers.append('Content-Type', 'application/json' );
    //headers.append('Content-Type', 'application/x-www-form-urlencoded');
    //headers.append('X-CSRF-Token', window.user_session.token );
    var options = new RequestOptions({ headers: headers });
    this.http.post(SERVER_URL+'/order/place.json',body , options)
    .subscribe(
      data=>this.gotResponseFromOrderRequest(data),
      err => this.logError(err.json().message),
      () => console.log('order placed')
    );
    this.stepNumber=0;
    window.avoidLoading=false
  }

  gotResponseFromOrderRequest(data){
    console.log(data);
    var parsed=JSON.parse(data._body)
    console.log(parsed);
    if(window.avoidLoading==false && parsed!="no_driver" && parsed!="has_debt"){
      window.loading= Loading.create({
        content: "Buscando conductor..."
      });
      this.nav.present(window.loading);
    }
    if (parsed.indexOf("no_driver")!=-1) {
      // let alert = Alert.create({
      //   title: 'Info',
      //   subTitle: 'Tu petición se ha pausado un momento en lo que un repartidor revisa tu petición, esto tardará solo un minuto.',
      //   buttons: ['OK']
      // });
      // this.nav.present(alert);
      if (window.thisOS!="macintel") {
        window.plugins.toast.show('Tu petición se ha pausado un momento en lo que un repartidor revisa tu petición, esto tardará solo un minuto.', 'long', 'center', function(a){console.log('toast success: ' + a)}, function(b){alert('toast error: ' + b)})  
      }
    }
    if (parsed.indexOf("has_debt")!=-1) {
    //   let alert = Alert.create({
    //   title: 'Deuda pendiente',
    //   subTitle: 'No se pudo colocar la orden, debido a que la última vez no efectuo el cobro',
    //   buttons: ['OK']
    // });
    // this.nav.present(alert);
    if (window.thisOS!="macintel") {
        window.plugins.toast.show('No se pudo colocar la orden, debido a una deuda pendiente que la última vez no se realizó el cobro, intenta usando una tarjeta nueva en tu perfil de usuario.', 'long', 'center', function(a){console.log('toast success: ' + a)}, function(b){alert('toast error: ' + b)})  
      }
    }
    window.avoidLoading=false;
  }
  getOrderInProgress(){
    if(NEO==false){
      this.http.get(SERVER_URL+'/request/inprogress.json')
      .subscribe(
        data=>this.gotOrderInProgress(data),
        err => this.logError(err.json().message),
        () => console.log('gps Complete')
      );  
    } else {
      console.log("accepted--->"+this.currentUser.content.id)
      this.http.get(SERVER_URL+'/TruckFinder/AcceptedOrdersByTruckId/'+this.currentUser.content.id)
      .subscribe(
        data=>this.gotOrderInProgress(data),
        err => this.logError(err.json().message),
        () => console.log('ACCEPTED-LIST')
    );
    }
  }

  gotOrderInProgress(data){
    if (NEO==true) {
      //window.currentOrderInDispatch={id:data.order,points:data.points};
      //var preparse="["+data._body.replace(/'/g,'"')+"]";
      var incoming=JSON.parse(data._body).Table[0];
      if (incoming!=undefined) {
        window.currentOrderInDispatch={order:incoming.idOrder,id:incoming.idOrder,points:incoming.points}
        var storage = window.localStorage;
        var mroute = storage.getItem("route"); // Pass a key name to get its value.
        if (mroute!=undefined || mroute!="") {
          window.currentOrderInDispatch.points=mroute;
          this.startProcessingRoute(mroute);    
        } else {
          window.currentOrderInDispatch.points=incoming.points.replace(/'/g,'"');
          this.startProcessingRoute(incoming.points.replace(/'/g,'"'));    
        }
      }
    } else {
      if (data._body.indexOf("NONE")==-1) {
        window.currentOrderInDispatch=JSON.parse(data._body).order;
        window.currentOrderInDispatch.points=JSON.parse(data._body).points;
        this.startProcessingRoute(JSON.parse(data._body).points);    
      } else {
        this._directionsRenderer.setMap(null);
      }
      
    }
    this.getETA();
    //this.captureGPS();
  }

  startProcessingRoute(points){
    this._directionsRenderer.setMap(this.map);
    
    var storage = window.localStorage;
    storage.setItem("route", points);

    this.statusState=3;
    var puntos=JSON.parse(points);
    if (puntos==null) {
      return;
    }
    
    
    var correctedParsed=[];
    for (var i=0;i< puntos.length;  i++) {
      puntos[i]
      correctedParsed.push(new google.maps.LatLng(puntos[i].lat, puntos[i].lng))
    }
    this._mapPoints=correctedParsed;
    this.getRoutePointsAndWaypoints(correctedParsed);
  }

  getRoutePointsAndWaypoints(Points) {
      if (Points.length <= 10) {
          this.drawRoutePointsAndWaypoints(Points);
      }
      else {
          var newPoints = new Array();
          var startPoint = Points.length - 10;
          var Legs = Points.length - 10;
          for (var i = startPoint; i < Points.length; i++) {
              newPoints.push(Points[i]);
          }
          this.drawRoutePointsAndWaypoints(newPoints);
          this.drawPreviousRoute(Legs);
      }

      //un delay para mostrar la ruta general y luego regresar al nivel normal
      setTimeout(() => this.map.setZoom(17), 10000);
      
  }
 
 
  drawRoutePointsAndWaypoints(Points) {
      //Define a variable for waypoints.
      var _waypoints = new Array();
   
      if (Points.length > 2) //Waypoints will be come.
      {
          for (var j = 1; j < Points.length - 1; j++) {
              var address = Points[j];
              if (address !== "") {
                  _waypoints.push({
                      location: address,
                      stopover: true  //stopover is used to show marker on map for waypoints
                  });
              }
          }
          //Call a drawRoute() function
          this.drawRoute(Points[0], Points[Points.length - 1], _waypoints);
      } else if (Points.length > 1) {
          //Call a drawRoute() function only for start and end locations
          this.drawRoute(Points[this._mapPoints.length - 2], Points[Points.length - 1], _waypoints);
      } else {
          //Call a drawRoute() function only for one point as start and end locations.
          this.drawRoute(Points[this._mapPoints.length - 1], Points[Points.length - 1], _waypoints);
      }
  }

  drawRoute(originAddress, destinationAddress, _waypoints) {
      //Define a request variable for route .
      var _request = '';
   
      //This is for more then two locatins
      if (_waypoints.length > 0) {
          _request = {
              origin: originAddress,
              destination: destinationAddress,
              waypoints: _waypoints, //an array of waypoints
              optimizeWaypoints: true, //set to true if you want google to determine the shortest route or false to use the order specified.
              travelMode: google.maps.DirectionsTravelMode.DRIVING
          };
      } else {
          //This is for one or two locations. Here noway point is used.
          _request = {
              origin: originAddress,
              destination: destinationAddress,
              travelMode: google.maps.DirectionsTravelMode.DRIVING
          };
      }
   
      //This will take the request and draw the route and return response and status as output
      this.directionsService.route(_request, (_response,_status)=> {
          if (_status == google.maps.DirectionsStatus.OK) {
            window.requesterScreenjs._directionsRenderer.setDirections(_response);
          }
      });
  }

  getETA(){
    
    
      if (window.currentOrderInDispatch==undefined && window.currentOrderInDispatch==null) {
        this.titleOrETA="Servicios";
        return;
      }
      var storage = window.localStorage;
      var mroute = storage.getItem("route");
      if (window.GPSPosition==undefined) {
        return;
      }
      var mforigin=window.GPSPosition.coords.latitude+","+window.GPSPosition.coords.longitude;
      var mfdestination=null;
      if (window.currentOrderInDispatch.points==null || window.currentOrderInDispatch.points=="null" || window.currentOrderInDispatch.points==undefined) {
        return;
      }
      if (this.statusState<7) {
        mfdestination=JSON.parse(window.currentOrderInDispatch.points)[1].lat+","+JSON.parse(window.currentOrderInDispatch.points)[1].lng;
      } else {
        mfdestination=JSON.parse(window.currentOrderInDispatch.points)[2].lat+","+JSON.parse(window.currentOrderInDispatch.points)[2].lng;
      }
      this.http.get("https://maps.googleapis.com/maps/api/distancematrix/json?origins="+mforigin+"&destinations="+mfdestination+"&key=AIzaSyBwdKhkBS0VwUhiX8beHePaHmjV6WIuVTs")
      .subscribe(
        data=>this.gotETA(data),
        err => this.logError(err.json().message),
        () => console.log('gps Complete')
    );

  }
  gotETA(data){
    console.log(data)
    var travelInformation=JSON.parse(data._body);
    if(window.currentOrderInDispatch==null || window.currentOrderInDispatch==undefined){
      this.titleOrETA="Servicios";
      return;
    }
    this.titleOrETA=travelInformation.rows[0].elements[0].duration.text+" "+travelInformation.rows[0].elements[0].distance.text;
  }

  loadMap(){
 	
        var minZoomLevel = 16;
        var currentPos=new google.maps.LatLng(25.67593049605625,-100.41711058462803);
        if(window.GPSPosition!=undefined){
          currentPos = new google.maps.LatLng(window.GPSPosition.coords.latitude,window.GPSPosition.coords.longitude);
        }

        this.map = new google.maps.Map(document.getElementById('map'), {
            zoom: minZoomLevel,
            center: currentPos,
            mapTypeId: google.maps.MapTypeId.ROADMAP
            // styles:window.empakingStyle
        });

        this._directionsRenderer.setMap(this.map);
        this._directionsRenderer.setOptions({
            //draggable: true
        });
        
    
        this.centerMarker = new google.maps.Marker({
          position: currentPos,
          map: this.map,
          title: 'Rastreo',
          icon:"img/flag-marker2.png",
          animation: google.maps.Animation.DROP,
        });

        this.map.addListener('center_changed', ()=> {
          // 3 seconds after the center of the map has changed, pan back to the
          // marker.
          // window.setTimeout(function() {
          //   map.panTo(marker.getPosition());
          // }, 3000);
          this.centerMarker.setPosition(this.map.getCenter());
        });
        setTimeout(()=>{
          this.getOrderInProgress();
          google.maps.event.trigger(this.map, 'resize');
        }, 1000);
  }

}
