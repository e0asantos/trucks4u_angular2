import {Page, NavController} from 'ionic-angular';

/*
  Generated class for the ShowOrdersPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Page({
  templateUrl: 'build/pages/show-orders/show-orders.html',
})
export class ShowOrdersPage {
  static get parameters() {
    return [[NavController]];
  }

  constructor(nav) {
    this.nav = nav;
  }
}
