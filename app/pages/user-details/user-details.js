import {Page,Alert,NavController,NavParams,Loading} from 'ionic-angular';
import { Inject } from '@angular/core';
import {Http, RequestOptions,Headers} from '@angular/http';
import {SERVER_URL} from '../../services/server-config'
import {user_session} from '../../services/server-config'

/*
  Generated class for the UserDetailsPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Page({
  templateUrl: 'build/pages/user-details/user-details.html',
})
export class UserDetailsPage {
  static get parameters() {
    return [[NavController],[Http],[NavController]];
  }

  constructor(nav,http,controller) {
    this.nav=nav;
    window.userDetailsjs=this;
    this.http=http;
    this.controller=controller;
    this.currentUser=window.user_session;
    this.usercoin=window.user_session.content;
    this.getUserDetailsFromServer();
  }

  getUserDetailsFromServer(){
    
    
      this.http.get(SERVER_URL+'/usuarios/me.json')
      .subscribe(
        data=> this.gotResponseForMyAccount(data),
        err => this.logError(err.json().message),
        () => console.log('gps Complete')
      );
  }
  logError(data){
    console.log(data)
  }
  success_callbak(data){
    console.log(data);
    window.userDetailsjs.usercoin.deviceToken=data.data.id;
    window.userDetailsjs.attemptUpdate();
  }
  error_callbak(data){
    console.log(data);
    let alert = Alert.create({
          title: 'Error',
          subTitle: data.data.description,
          buttons: ['OK']
        });
    window.userDetailsjs.nav.present(alert);
  }
  reUpdateExpiration(){
    setTimeout(() => {
      this.getMonthOnly();
      this.getYearOnly();
    }, 1000);
  }
  getMonthOnly(){
    if (this.usercoin.cardexpire!=undefined && this.usercoin.cardexpire!=null) {
      this.usercoin.expiremonth=this.usercoin.cardexpire.split("-")[1];
    }
    
    
  }
  getYearOnly(){
    if (this.usercoin.cardexpire!=undefined && this.usercoin.cardexpire!=null) {
      this.usercoin.expireyear=this.usercoin.cardexpire.split("-")[0].substr(2);
    }
    
  }
  preAttemptUpdate(){
    if(this.usercoin.card!=undefined && this.usercoin.card!=""
      && this.usercoin.cardexpire!=undefined && this.usercoin.cardexpire!=""
      && this.usercoin.ccv!=undefined && this.usercoin.ccv!=""){
      OpenPay.token.extractFormAndCreate('customer-form', this.success_callbak, this.error_callbak);  
    } else {
      this.attemptUpdate();
    }
    
  }
  attemptUpdate(){
    if (this.usercoin.name!=undefined && this.usercoin.name!=""
      && this.usercoin.lastname!=undefined && this.usercoin.lastname!=""
      && this.usercoin.mobile!=undefined && this.usercoin.mobile!=""
      && this.usercoin.email!=undefined && this.usercoin.email!=""
      && this.usercoin.hashseed!=undefined && this.usercoin.hashseed!=""
      && this.usercoin.birthday!=undefined && this.usercoin.birthday!=""
      ) {
        this.usercoin.authenticity_token=window.TOKEN;
        var body = JSON.stringify(this.usercoin);
        var headers = new Headers({ 'Content-Type': 'application/json' });
        var options = new RequestOptions({ headers: headers });
        this.http.patch(SERVER_URL+'/villagers/'+this.usercoin.id+'.json',body , options)
        .subscribe(
          data=>this.gotResponseFromRegistration(data),
          err => this.logError(err.json().message),
          () => console.log('order placed')
        );
        this.stepNumber=0;
        } else {
          let alert = Alert.create({
          title: 'Error',
          subTitle: 'Antes de intentar guardar cambios, verifica que la información este completa',
          buttons: ['OK']
        });
      this.nav.present(alert);
    }
    // this.http.post(SERVER_URL+'/order/place.json',body , options)
    // .subscribe(
    //   data=>this.gotResponseFromOrderRequest(data),
    //   err => this.logError(err.json().message),
    //   () => console.log('order placed')
    // );
  }
  gotResponseFromRegistration(data){
    console.log(data);
    window.masterGPSTimeout=undefined;
    let alert = Alert.create({
          title: 'información',
          subTitle: 'Los cambios se han guardado',
          buttons: ['OK']
        });
      this.nav.present(alert);
    window.user_session.content=JSON.parse(data._body);
  }
  gotResponseForMyAccount(data){
    console.log(data)
    window.user_session.content=JSON.parse(data._body);
    // this.currentUser=JSON.parse(data._body);
    // this.usercoin=JSON.parse(data._body);
  }
}
