import {Page, NavController} from 'ionic-angular';
import {LoginScreen} from '../loginScreen/loginScreen';

@Page({
  templateUrl: 'build/pages/welcomeScreen/welcomeScreen.html'
})
export class WelcomeScreen {
   
   static get parameters() {
    return [[NavController]];
  }

  constructor(nav) {
    this.nav = nav;
 
  }
  
  neverShowAgain(){
  	var storage = window.localStorage;
    var dbGPS = storage.getItem("welcome1"); // Pass a key name to get its value.
    storage.setItem("welcome", true);
    this.nav.setRoot(LoginScreen);
  }
}
