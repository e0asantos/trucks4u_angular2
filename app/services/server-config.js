function isProduction()
{
    // Rudimentary check to see if we are running on Heroku. Should provide a more flexible config.
    var wn = window.navigator;
    window.thisOS = wn.platform.toString().toLowerCase();
    return window.thisOS=="macintel";
}
export let SERVER_URL = isProduction() ? "http://localhost:8100/api" : "https://www.unfavor.today";
export let NEO=false;
export var user_session=Object();